import * as moment from "moment";
import "moment/locale/ru";
import CONSTANTS from "../consts/constants";

export const dateSpaceFormat = "DD MMMM YYYY";
export const dateDotFormat = "DD.MM.YYYY";
export const dateDashFormat = "YYYY-MM-DD";
export const timeFormat = "HH:mm:ss";
export const timeFormatWithoutSec = "HH:mm";
export const dateDayMonthFormat = "DD MMMM";
export const reportDateFormat = "DD:MM:YYYY HH:mm";

export const dateTimeDotFormat = `${dateDotFormat} ${timeFormat}`;
export const dateTimeDotFormatWithoutSec = `${dateDotFormat} ${timeFormatWithoutSec}`;
export const dateTimeFormatStandart = `${dateDashFormat}T${timeFormat}`;

export const getUtcDate = (date) => {
  return moment.utc(date);
};
export const getCurrentMomentDate = (date) => {
  return moment(date);
};

export const formateDate = (date, format, options) => {
  let dateMoment = options?.utc ? getUtcDate(date) : getCurrentMomentDate(date);

  if (dateMoment.isValid()) {
    if (options?.local) {
      dateMoment = dateMoment.local();
    }

    return dateMoment.format(format);
  }
  return "";
};

export const getFormattedDateWithSpace = (date, local) => {
  return formateDate(date, dateSpaceFormat, {local});
};

export const getFormattedDate = (date, local) => {
  return formateDate(date, dateDotFormat, {local});
};

export const getFormattedDashDate = (date, local, utc) => {
  return formateDate(date, dateDashFormat, {local, utc});
};

export const getFormattedDayMonthDate = (date, local) => {
  return formateDate(date, dateDayMonthFormat, {local});
};

export const getFormattedDateTime = (date, local, withoutSec) => {
  return formateDate(date, withoutSec ? dateTimeDotFormatWithoutSec : dateTimeDotFormat, {
    local,
  });
};

export const getDateFromUtcToLocal = (date) => {
  return getUtcDate(date).local();
};

export const getFormattedDateTimeStandart = (date, local) => {
  return formateDate(moment(date).utc(), dateTimeFormatStandart, {local});
};
export const getFormattedUtcDateTimeStandart = (date, options) => {
  return formateDate(date, dateTimeFormatStandart, options);
};

export const setMomentUnit = (date, unit, count) => {
  return formateDate(getCurrentMomentDate(date).set(unit, count));
};

export const diffDates = (dateA, dateB, typeDiff, float = true) => {
  return moment(dateA).diff(moment(dateB), typeDiff, float);
};

export const diffDatesByDays = (dateA, dateB) => {
  return diffDates(getFormattedDashDate(dateA), getFormattedDashDate(dateB), "days");
};

export const set210000YesterdayIfNotToday = (date) => {
  const diffInDays = diffDatesByDays(getCurrentMomentDate(), date);

  if (diffInDays > CONSTANTS.COUNT.ZERO) {
    return setDateTime210000(dateSubtractDays(getCurrentMomentDate(date).clone(), 1));
  }

  const cloneDate = getCurrentMomentDate(date).clone();

  return setDateTimeCurrent(cloneDate);
};

export const dateSubtract = (date, amount, unit) => {
  return moment(date).subtract(amount, unit);
};

export const dateSubtractDays = (date, unit) => {
  return formateDate(moment(date).subtract(unit, "days"));
};

export const setDateTime210000 = (dateOwn) => {
  const date = moment(dateOwn);
  date.hours(21);
  date.minutes(0);
  date.seconds(0);

  return formateDate(date);
};

export const setDateTime2059 = (dateOwn) => {
  const date = moment(dateOwn);
  date.hours(20);
  date.minutes(59);
  date.seconds(0);

  return formateDate(date);
};

export const setDateTimeCurrent = (dateOwn) => {
  const date = moment(dateOwn);
  date.hours(getCurrentMomentDate().get("hour"));
  date.minutes(getCurrentMomentDate().get("minutes"));
  date.seconds(0);

  return formateDate(date);
};

export const checkToday21AndSubtract = (dateOwn) => {
  const date = moment.utc(dateOwn);
  const diffInDays = diffDatesByDays(getCurrentMomentDate(), dateOwn);
  let numberToSubtract = CONSTANTS.TIME.SIX_DAYS;

  //если не сегодня
  if (diffInDays > CONSTANTS.COUNT.ZERO) {
    return dateSubtractDays(dateOwn, numberToSubtract);
  }

  const countHours = date.hours();
  if (countHours >= CONSTANTS.TIME.TWENTY_ONE_HOUR_GRINWICH && countHours < 21) {
    numberToSubtract = CONSTANTS.TIME.FIVE_DAYS;
  }

  return dateSubtractDays(date, numberToSubtract);
};

export const getReportFormattedDate = (date) => {
  return formateDate(date, reportDateFormat, {local: true});
};

export const getLastCleaningDate = (dateOwn) => {
  let date = formateDate(dateOwn);

  if (diffDatesByDays(getCurrentMomentDate(), date) > CONSTANTS.COUNT.ZERO) {
    return setDateTime2059(date);
  } else if (diffDatesByDays(getCurrentMomentDate(), date) === CONSTANTS.COUNT.ZERO) {
    const hours = getCurrentMomentDate().hours();

    if (hours >= 21) {
      return setDateTime2059(date);
    } else {
      return setDateTimeCurrent(date);
    }
  }

  return date;
};
