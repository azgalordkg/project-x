export const getTitleObjectByCount = (count) => {
  // if (count % 10 === 0) {
  //   return 'объектов';
  // }
  // if (count % 10 === 1) {
  //   if (count % 100 === 11) {
  //     return 'объектов';
  //   }
  //
  //   return 'объект';
  // }
  // if (count % 10 === 2) {
  //   if (count % 100 === 12) {
  //     return 'объектов';
  //   }
  //
  //   return 'объекта';
  // }
  // if (count % 10 === 3) {
  //   if (count % 100 === 13) {
  //     return 'объектов';
  //   }
  //
  //   return 'объекта';
  // }
  // if (count % 10 === 4) {
  //   if (count % 100 === 14) {
  //     return 'объектов';
  //   }
  //
  //   return 'объекта';
  // }
  //
  // return 'объектов';

  return declension(count, ["объект", "объекта", "объектов"]);
};

export const capitalize = (str) => {
  if (str) {
    return `${str.charAt(0).toUpperCase()}${str.slice(1)}`;
  }

  return str;
};

// This method will work with any expression in any count
export const declension = (num, expressions) => {
  let count = num % 100;

  if (count >= 5 && count <= 20) return expressions[2];

  count = count % 10;

  if (count === 1) return expressions[0];

  if (count >= 2 && count <= 4) return expressions[1];

  return expressions[2];
};
