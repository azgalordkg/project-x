import CONSTANTS from "../consts/constants";

export function saveData(blob, fileName) {
  if (blob === null || fileName === null) {
    return;
  }
  if (navigator.msSaveOrOpenBlob) {
    navigator.msSaveOrOpenBlob(blob, fileName || "Отчет.xls");
  } else {
    const url = window.URL.createObjectURL(blob);

    const a = document.createElement("a");
    a.style.display = "none";
    a.href = url;
    a.download = fileName || "Отчет.xls";

    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    setTimeout(() => window.URL.revokeObjectURL(url), CONSTANTS.TIME.ONE_SECOND_IN_MS);
  }
}
