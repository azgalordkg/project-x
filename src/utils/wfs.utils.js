export function genILIKEFilter(key, val) {
  return `(${key} ILIKE '${val}')`;
}

export function genEQUALSFilter(key, val) {
  return `(${key}=${val})`;
}
