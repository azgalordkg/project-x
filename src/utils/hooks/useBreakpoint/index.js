import * as React from "react";
import {useDispatch} from "react-redux";
import {setBreakpoint} from "../../../store/app/actions";

let BREAKPOINTS = {};

function initMatchMedia(breakpointSetter) {
  const handleMediaChange = (breakpoint) => (mq) => {
    if (mq.matches) {
      breakpointSetter(breakpoint);
    }
  };

  const LAPTOP = window.matchMedia(`(max-width: ${1280 - 1}px)`);
  const DESKTOP = window.matchMedia(
    `(min-width: ${1280}px) and (max-width: ${1920 - 1}px)`
  );
  const WIDE = window.matchMedia(`(min-width: ${1920}px)`);
  LAPTOP.addListener(handleMediaChange("LAPTOP"));
  DESKTOP.addListener(handleMediaChange("DESKTOP"));
  WIDE.addListener(handleMediaChange("WIDE"));
  BREAKPOINTS = {LAPTOP, DESKTOP, WIDE};

  Object.keys(BREAKPOINTS).forEach((bpKey) => {
    handleMediaChange(bpKey)(BREAKPOINTS[bpKey]);
  });
}

const useBreakpoint = (deep) => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    initMatchMedia((arg) => dispatch(setBreakpoint(arg)));
  }, deep);
};

export default useBreakpoint;
