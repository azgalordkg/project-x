import * as React from "react";

export class ErrorBoundary extends React.PureComponent {
  state = {
    error: undefined,
    info: undefined,
  };

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({
      error,
      info,
    });
    console.warn(error); // eslint-disable-line no-console
  }

  render() {
    if (this.state.error) {
      return null;
    }
    return this.props.children;
  }
}
