import {
  ROLES,
  SEASONS_FOR_MER,
  LEGEND_PERMISSIONS,
} from "../consts/permissions/constants";

export const checkRole = (user) => {
  let result = ROLES.UNKNOWN;
  user.roles.some((role) => {
    const findedRole = Object.keys(ROLES).find(
      (key) => role.includes(key) && !role.includes(ROLES.ROLE_ADMIN)
    );
    if (findedRole) {
      result = findedRole;
    }
    return findedRole;
  });
  return result;
};

export const checkIsAdmin = (user) => {
  const userRole = checkRole(user);
  return userRole === ROLES.ROLE_DASHBORD_ADMINISTRATOR;
};

export const checkIsMer = (user) => {
  const userRole = checkRole(user);
  return userRole === ROLES.ROLE_DASHBORD_MER;
};

const MER_OR_ADMIN = new Set([
  ROLES.ROLE_DASHBORD_MER,
  ROLES.ROLE_DASHBORD_ADMINISTRATOR,
  ROLES.ROLE_ADMIN,
]);

export const checkIsMerOrAdminOrAdministrator = (user) => {
  const userRole = checkRole(user);

  return MER_OR_ADMIN.has(userRole);
};

export const getLengendByPermission = (legend, user, seasonActive) => {
  const falseValue = false;
  if (falseValue && checkIsMer(user) && SEASONS_FOR_MER.has(seasonActive)) {
    return legend.filter(
      (item) => !LEGEND_PERMISSIONS[ROLES.ROLE_DASHBORD_MER].includes(item.color)
    );
  }

  return [...legend];
};
