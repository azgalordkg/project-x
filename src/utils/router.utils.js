export const isEqualPathWithLocation = (location, path) => {
  return location.pathname === path;
};
