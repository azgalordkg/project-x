import queryString from "query-string";
import {isArray, isNullOrUndefined, isNumber} from "util";
import memoizeOne from "memoize-one";
import CONSTANTS from "../consts/constants";

export const useSearchMergeNewState = (searchState, data) => {
  const query = {
    ...searchState,
    ...data,
  };

  Object.entries(query).forEach(([key, data]) => {
    if (isNullOrUndefined(query[key])) {
      delete query[key];                              // eslint-disable-line
    } else {
      query[key] = JSON.stringify(data);
    }
  });

  return query;
};

export const makeObjFromMemoize = memoizeOne((search) => {
  return Object.entries(queryString.parse(search)).reduce((newObj, [key, data]) => {
    if (isArray(data)) {
      data.forEach((dateItem) => {
        newObj[key] = [];
        newObj[key].push(JSON.parse(dateItem));
      });
    } else {
      newObj[key] = JSON.parse(data);
    }

    return newObj;
  }, {});
});

export const generatePathParams = (path, params, routeMatchParams) => {
  let urlAsArray = path.split("/").map((partOfUrl) => {
    let ans = partOfUrl.replace("?", "");

    if (params) {
      Object.entries(params).forEach(([key, value]) => {
        ans = ans.replace(`:${key}`, value || isNumber(value) ? value.toString() : "");
      });
    }

    if (routeMatchParams) {
      Object.entries(routeMatchParams).forEach(([key, value]) => {
        ans = ans.replace(`:${key}`, value ? value : "");
      });
    }

    return ans;
  });

  const emptyIndex = urlAsArray.findIndex((value, index) => index && !value);
  if (emptyIndex > CONSTANTS.INDEX_OF_ARR.FIRST_INDEX) {
    urlAsArray = urlAsArray.slice(CONSTANTS.INDEX_OF_ARR.FIRST_INDEX, emptyIndex);
  }

  return urlAsArray.join("/");
};

export const generatePathSearch = (search, routeLocationSearch) => {
  return queryString.stringify(
    useSearchMergeNewState(makeObjFromMemoize(routeLocationSearch), search)
  );
};

export const generateHistoryPath = (
  path,
  params,
  search,
  routeMatchParams,
  routeLocationSearch
) => {
  return `${generatePathParams(path, params, routeMatchParams)}?${generatePathSearch(
    search,
    routeLocationSearch
  )}`;
};
