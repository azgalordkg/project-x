import VectorSource from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";
import LayerVectorImage from "ol/layer/VectorImage";
import {GEOJSON} from "./geojson";

export const createGeometryLayer = (name, style, zIndex) => {
  const vectorSource = new VectorSource({format: GEOJSON});

  const layer = new VectorLayer({
    source: vectorSource,
    style,
    zIndex,
  });

  layer.set("name", name);
  layer.set(name, true);

  return layer;
};

export const createGeometryLayerImage = (name, style, zIndex) => {
  const vectorSource = new VectorSource({format: GEOJSON});

  const layer = new LayerVectorImage({
    source: vectorSource,
    style,
    zIndex,
  });

  layer.set("name", name);
  layer.set(name, true);

  return layer;
};
