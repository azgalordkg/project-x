const defaultDuration = 1000;

const LastFitId = Symbol("meta");

export const fitExtent = (map, extent, options, callBack) => {
  clearTimeout(map[LastFitId]);

  map[LastFitId] = setTimeout(() => {
    map.getView().fit(extent, {duration: defaultDuration, ...options});
    if (callBack) {
      setTimeout(() => {
        callBack();
      }, options.duration || defaultDuration);
    }
  }, 100);
};
