import * as React from "react";
import {Route, Redirect} from "react-router-dom";
import {useSelector} from "react-redux";

import {selectedLoggedIn, selectLogin} from "../store/app/selectors";
import useBreakpoint from "../utils/hooks/useBreakpoint";

const PrivateRoute = React.memo(({component: Component, ...rest}) => {
  const loggedIn = useSelector(selectedLoggedIn);
  const login = useSelector(selectLogin);
  useBreakpoint([loggedIn, login]);

  return (
    <Route
      {...rest}
      render={(props) => {
        if (!loggedIn || !login) {
          return <Redirect to="/login" />;
        } else {
          return <Component {...props} />;
        }
      }}
    />
  );
});

PrivateRoute.displayName = "PrivateRoutes";

export default PrivateRoute;
