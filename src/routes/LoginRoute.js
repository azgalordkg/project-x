import * as React from "react";
import {Redirect} from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";
import {isString} from "util";

import LoginForm from "../containers/LoginForm";
import {
  selectedLoggedIn,
  selectLogin,
  selectError,
  loginLoading,
} from "../store/app/selectors";
import {loginSaga} from "../store/app/actions";
import {SIDEBAR_TABS} from "../consts/sidebar/constants";

const LoginPageFormWrap = React.memo(() => {
  const loggedIn = useSelector(selectedLoggedIn);
  const login = useSelector(selectLogin);
  const error = useSelector(selectError);
  const loginLoadingValue = useSelector(loginLoading);

  const dispatch = useDispatch();

  const onSubmit = React.useCallback((user) => dispatch(loginSaga(user)), []);

  if (!!loggedIn && !!login) {
    return <Redirect to={SIDEBAR_TABS.MAP.path} />;
  }

  return (
    <LoginForm
      loginLoading={loginLoadingValue}
      onSubmit={onSubmit}
      errorMessage={error ? <div>{`Ошибка. ${isString(error) ? error : ""}`}</div> : ""}
    />
  );
});

LoginPageFormWrap.displayName = "LoginPageFormWrap";

export default LoginPageFormWrap;
