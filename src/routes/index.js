import * as React from "react";
import {Router, Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
// local files
import {historyDashboard} from "../store/utils/history";
import Loading from "../components/Loading";
import {selectAppGlobalIsLoading} from "../store/app/selectors";
import LoginRoute from "./LoginRoute";
import PrivateRoute from "./PrivateRoute";
import AppRoute from "./AppRoute";

const RouterContainer = React.memo(() => {
  const appGlobalIsLoading = useSelector(selectAppGlobalIsLoading);

  return (
    <React.Fragment>
      <Router history={historyDashboard}>
        <Switch>
          <Route path="/login" component={LoginRoute} />
          <PrivateRoute path="/" component={AppRoute} />
        </Switch>
      </Router>
      {appGlobalIsLoading && (
        <Loading type="new_spinner" overall backgroundColor="rgba(0, 0, 0, 0.5)" />
      )}
    </React.Fragment>
  );
});

RouterContainer.displayName = "RouterContainer";

export default RouterContainer;
