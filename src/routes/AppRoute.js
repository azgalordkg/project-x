import * as React from "react";
import {Route, Switch} from "react-router-dom";
// local files
import {MapContainer} from "../containers/MapContainer";
import {TableContainer} from "../containers/TableContainer";
import {ChartContainer} from "../containers/ChartContainer";
import {AppLayout} from "../layouts/AppLayout";
import {SIDEBAR_TABS} from "../consts/sidebar/constants";

const AppRoute = React.memo(() => {
  return (
    <Switch>
      <AppLayout>
        <Route path="/" exact component={MapContainer} />
        <Route path={SIDEBAR_TABS.TABLE.path} component={TableContainer} />
        <Route path={SIDEBAR_TABS.DASHBOARD.path} component={ChartContainer} />
      </AppLayout>
    </Switch>
  );
});

AppRoute.displayName = "AppRoute";

export default AppRoute;
