import * as React from "react";
import {useSelector, useDispatch} from "react-redux";
import {Modal, Button} from "antd";

import {selectModalSessionExpired} from "../../store/app/selectors";
import {logoutBySessionExpiredSaga} from "../../store/app/actions";

const ModalSessionExpired = React.memo(() => {
  const isOpen = useSelector(selectModalSessionExpired);

  const dispatch = useDispatch();

  const onClose = React.useCallback(() => dispatch(logoutBySessionExpiredSaga()), []);

  if (!isOpen) return null;

  return (
    <React.Fragment>
      <Modal
        title="Время сессии истекло"
        centered={true}
        visible
        onOk={onClose}
        onCancel={null}
        closable={false}
        footer={
          <React.Fragment>
            <Button className="button__ok" type="primary" key="back" onClick={onClose}>
              Ок
            </Button>
          </React.Fragment>
        }
      >
        <p>Вы будете перенаправлены на страницу авторизации</p>
      </Modal>
    </React.Fragment>
  );
});

ModalSessionExpired.displayName = "ModalSessionExpired";

export default ModalSessionExpired;
