import * as React from "react";
import {Form, Input, Button} from "antd";
import {EyeFilled, EyeOutlined} from "@ant-design/icons";

const FormItem = Form.Item;

class LoginPageForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      type: "password",
    };
  }

  // Обработчик клика по вложенному суффиксу поля ввода
  createInputSuffix = () => {
    const {type} = this.state;
    const Icon = type === "password" ? EyeFilled : EyeOutlined;

    return <Icon className="login-eye" onClick={this.handleClickByPasswordInputSuffix} />;
  };

  // Обработчик клика по вложенному суффиксу поля ввода
  handleClickByPasswordInputSuffix = () => {
    this.setState((prevState) => ({
      type: prevState.type === "password" ? "text" : "password",
    }));
  };

  render() {
    const {type} = this.state;
    const {loginLoading, errorMessage} = this.props;
    const suffix = this.createInputSuffix();
    const initialValues = {login: "", password: ""};

    return (
      <div className="login-container login-container-wrap">
        <div className="login-form-container">
          <Form
            name="basic"
            initialValues={initialValues}
            className="login-form login-form__login"
            onFinish={this.props.onSubmit}
          >
            <div className="login-logo-wrap">
              <div className="login-logo login-logo__login" />
            </div>

            <div className="login-title__container">
              <p className="login-title__text">
                Мониторинг
                <br /> чистоты
                <br /> города
              </p>
            </div>

            {/* Пока под вопросом будет ли */}
            {/* <LinkWrapStyled>
              <LinkStyled to={`/update-credentials`}>{'Сменить пароль'}</LinkStyled>
            </LinkWrapStyled> */}

            <FormItem
              name="login"
              rules={[{required: true, message: "введите имя пользователя"}]}
            >
              <Input
                id="login"
                className="login-input login-input__left"
                placeholder="Логин"
              />
            </FormItem>
            <FormItem
              name="password"
              rules={[{required: true, message: "введите пароль"}]}
            >
              <Input
                className="login-input login-input__left"
                type={type}
                placeholder="Пароль"
                suffix={suffix}
              />
            </FormItem>
            <FormItem style={{marginTop: "40px"}}>
              <Button
                loading={loginLoading}
                style={{height: "40px", width: "200px"}}
                htmlType="submit"
                className="login-form-button__login"
              >
                Войти
              </Button>
            </FormItem>

            {errorMessage && (
              <Form.Item className="login-errors">
                <div className="ant-form-explain" style={{color: "#f5222d"}}>
                  {errorMessage}
                </div>
              </Form.Item>
            )}

            <div className="login-form-support__login">
              <div className="login-footer-text login-footer-text__login">
                <a href="mailto:helpdesk-monitor@mos.ru">{"Техническая поддержка"}</a>
              </div>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default LoginPageForm;
