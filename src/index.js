import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./styles/ant.less";
import "./styles/main.scss";

ReactDOM.render(<App />, document.getElementById("root"));
