import * as React from "react";
import {Provider, useSelector, useDispatch} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";
import {notification, ConfigProvider} from "antd";
import {hot} from "react-hot-loader/root";
import ruRU from "antd/lib/locale-provider/ru_RU";
// local
import {configureStore} from "./store/configure-store";
import {selectAuth} from "./store/app/selectors";
import {loginSaga} from "./store/app/actions";
import Loading from "./components/Loading";
import RouterContainer from "./routes";

ruRU.Transfer = {
  ...ruRU.Transfer,
  itemUnit: "",
  itemsUnit: "",
};

ruRU.Modal = {
  ...ruRU.Modal,
  cancelText: "Отмена",
  justOkText: "OK",
  okText: "OK",
};

notification.config({top: 50});
window.notification = notification;

const App = () => {
  const auth = useSelector(selectAuth);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(loginSaga(null));
  }, []);

  if (!auth?.checked) {
    return <Loading />;
  }

  return <RouterContainer />;
};

const AppContent = hot(App);

const AppWrapper = () => {
  const storeInit = configureStore();
  return (
    <Provider store={storeInit.store}>
      <PersistGate loading={<div>Загрузка...</div>} persistor={storeInit.persistor}>
        <React.Suspense fallback={<Loading />}>
          <ConfigProvider locale={ruRU}>
            <AppContent />
          </ConfigProvider>
        </React.Suspense>
      </PersistGate>
    </Provider>
  );
};

export default AppWrapper;
