import React from "react";
import {Button} from "antd";
import {CalendarFilled, LoginOutlined} from "@ant-design/icons";
import {NavLink} from "react-router-dom";

export const Header = () => {
  return (
    <div className="header">
      <div className="header-row header-row__wrap">
        <div>
          <span className="header-filters-label">Сезон</span>
          <Button className="button__primary active">Зима</Button>
          <Button className="button__primary">Лето</Button>
        </div>
        <div>
          <span className="header-filters-label">Нарушения</span>
          <Button className="button__primary active">Критичные</Button>
          <Button className="button__primary">Некритичные</Button>
          <Button className="button__primary">Систематические</Button>
          <Button className="button__primary">За период</Button>
        </div>
        <div>
          <Button className="button__primary active">Сейчас</Button>
          <Button>
            <CalendarFilled />
          </Button>
        </div>
        <div className="header-row">
          <span>31.08.2020 18:34</span>
          <div className="header-theme-checkbox" />
        </div>
        <div>
          <NavLink className="header-navlink" exact to="/dashboard">
            <Button className="button__primary">Дашборд</Button>
          </NavLink>
          <NavLink className="header-navlink" exact to="/table">
            <Button className="button__primary">Таблица</Button>
          </NavLink>
          <NavLink className="header-navlink" exact to="/">
            <Button className="button__primary">Карта</Button>
          </NavLink>
        </div>
        <div>
          <span>Мэр</span>
          <Button>
            <LoginOutlined />
          </Button>
          <Button>?</Button>
        </div>
      </div>

      <div className="header-row header-row__wrap">
        <div>
          <span className="header-filters-label">Источники</span>
          <Button className="button__primary active">ОАТИ 2 (0.26%)</Button>
          <Button className="button__primary">ЦОДД 0 (0.00%)</Button>
          <Button className="button__primary">ЕДЦ 2 (0.26%)</Button>
          <Button className="button__primary">ЦАФАП 0 (0.00%)</Button>
          <Button className="button__primary">НГ 2 (0.26%)</Button>
          <Button className="button__primary">МЖИ 9 (1.18%)</Button>
        </div>
        <div>
          <span className="header-filters-label">От проверенных</span>
        </div>
      </div>
      <div className="header-row header-row__wrap">
        <div>
          <span className="header-filters-label">Объекты</span>
          <Button className="button__primary active">Дворы 0 (0.00%)</Button>
          <Button className="button__primary">Дороги 4 (16.00%)</Button>
          <Button className="button__primary">Соцобъекты 0 (0.00%)</Button>
          <Button className="button__primary">Дома 11 (2.04%)</Button>
          <Button className="button__primary">ТПУ 0 (0.00%)</Button>
          <Button className="button__primary">Парки, скверы 0 (0.00%)</Button>
        </div>
        <div>
          <Button className="button__primary">Контейнеры</Button>
        </div>
      </div>
      <div className="header-row header-row__wrap">
        <div>
          <span className="header-filters-label">Ответственные</span>
          <Button className="button__primary active">Федеральные 0 (0.00%)</Button>
          <Button className="button__primary">Территориальные 0 (0.00%)</Button>
          <Button className="button__primary">Отраслевые 0 (0.00%)</Button>
          <Button className="button__primary">ЧУК/Иные 0 (0.00%)</Button>
        </div>
        <div>
          <Button className="button__primary">Департаменты</Button>
        </div>
      </div>
      <div className="header-row header-row__wrap">
        <div>
          <span className="header-filters-label">Активные фильтры</span>
        </div>
        <div>
          <span className="header-tag">Территориальные</span>
        </div>
        <Button className="button__reset">Сбросить фильтры</Button>
      </div>
    </div>
  );
};
