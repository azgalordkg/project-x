import React from "react";
import {Header} from "./Header";

export const AppLayout = ({children}) => {
  return (
    <React.Fragment>
      <Header />
      {children}
    </React.Fragment>
  );
};
