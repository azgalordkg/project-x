import {
  URL_WIDGETS,
  URL_FOR_TABLE,
  URL_INITIAL_FILTERS,
  URL_DASHBOARD_DASHLET,
  URL_FOR_TABLE_IN_EXCEL,
  URL_EXECUTORS,
  URL_SUBORDINATION_TYPE_REPRESENTATIVES,
  URL_REGIONS,
  URL_DISTRICTS,
  URL_DEFECT_TEXT,
} from "./constants";
import {axiosDashboard} from "./axios";

export const getInitialFilters = () => {
  return axiosDashboard.get(URL_INITIAL_FILTERS);
};

export const getDashboardData = (bodyParams, params) => {
  return axiosDashboard.post(URL_WIDGETS, bodyParams, {params});
};

export const getDashboardTableData = (bodyParams, params) => {
  return axiosDashboard.post(URL_FOR_TABLE, bodyParams, {params});
};

export const getDashboardTableExcel = (bodyParams, params) => {
  return axiosDashboard.post(URL_FOR_TABLE_IN_EXCEL, bodyParams, {
    params,
    responseType: "blob",
  });
};

export const getDashboardDashletData = (params) => {
  return axiosDashboard.get(URL_DASHBOARD_DASHLET, {params});
};

export const getExecutors = () => {
  return axiosDashboard.get(URL_EXECUTORS);
};

export const getSubordinationTypeRepresentatives = () => {
  return axiosDashboard.get(URL_SUBORDINATION_TYPE_REPRESENTATIVES);
};

export const getRegions = () => {
  return axiosDashboard.get(URL_REGIONS);
};

export const getDistricts = (payload) => {
  let URL = URL_DISTRICTS;
  if (payload.length) {
    const regId = "regionId=";
    const params = payload.join(`&${regId}`);
    URL = `${URL_DISTRICTS}?${regId}${params}`;
  }
  return axiosDashboard.get(URL);
};

export const getDefectText = (payload) => {
  let URL = URL_DEFECT_TEXT;
  if (payload) {
    URL += `?namePart=${payload}`;
  }
  return axiosDashboard.get(URL);
};
