import {axiosDashboard} from "./axios";
import {URL_FEATURES} from "./constants";
import {FeatureAliases} from "./types";

//base methods
//  TODO wrong swagger response type
export const loadFeatures = (alias) => axiosDashboard.get(`${URL_FEATURES}/${alias}`);

//  TODO wrong swagger response type
export const loadFeaturesByFilter = (alias, filter, value, params) =>
  axiosDashboard.get(`${URL_FEATURES}/${alias}/${filter}/${value}`, {params});

export const loadRegions = () => loadFeatures(FeatureAliases.REGION);

export const loadDistricts = (regionName) =>
  loadFeaturesByFilter(FeatureAliases.DISTRICT, "properties.parent", regionName);

export const loadCity = () => loadFeatures(FeatureAliases.CITY);
