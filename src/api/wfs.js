import {axiosDashboard} from "./axios";

export function getFeaturesWfs(layerId, revisionId, params, withPagination) {
  let paginationString = "";
  if (withPagination) {
    const {page, size} = withPagination;
    paginationString = `&page=${page}&size=${size}`;
  }
  return axiosDashboard.get(
    `/egip/layers/${layerId}/revisions/${revisionId}/features/wfs?srsType=EPSG_3857${paginationString}`,
    {params: {...params, size: 1000}}
  );
}
