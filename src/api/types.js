export const FeatureAliases = {
  CITY: "moscow.russia",
  REGION: "regions.moscow.russia",
  DISTRICT: "districts.regions.moscow.russia",
};

export const AvailableViolationSource = {
  GOVERNMENT: "GOVERNMENT",
  CITIZEN: "CITIZEN",
  SYS: "SYS",
  ASUPR: "ASUPR",
  ALL: "ALL",
};

export const WeatherPartsType = {
  DAY: "day",
  EVENING: "evening",
  MORNING: "morning",
  NIGHT: "night",
};
