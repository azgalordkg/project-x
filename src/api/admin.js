import {axiosDashboard} from "./axios";
import {PropertyType} from "../consts/types";
import {URL_ADMIN_FILTER} from "./constants";

export function getLayers(q) {
  let params = new URLSearchParams();
  if (q) {
    Object.keys(q).forEach((key) => {
      if (key === "fields") {
        q[key].forEach((item) => {
          params.append("field", item);
        });
      } else {
        params.append(key, q[key]);
      }
    });
  }
  return axiosDashboard
    .get("/api/v1/dashboard/admin/layers", {
      params,
    })
    .then((r) => r.data);
}

export function getLayerTypes() {
  return axiosDashboard
    .get("/api/v1/dashboard/admin/layers/layer_type")
    .then((r) => r.data);
}

export function editField(params) {
  return axiosDashboard.post("api/v1/dashboard/admin/layers/edit", params).then((r) => r);
}

export function editEnableField(params) {
  return axiosDashboard
    .post("api/v1/dashboard/admin/layers/enable", params)
    .then((r) => r);
}

export function getMapping(alias) {
  return axiosDashboard
    .get(`api/v1/dashboard/admin/layers/mapping/${alias}`)
    .then((r) => r.data);
}

export function deleteMappingField(id) {
  return axiosDashboard
    .delete(`api/v1/dashboard/admin/layers/mapping/${id}`)
    .then((r) => r);
}

export function editMappingField(params) {
  return axiosDashboard
    .post("api/v1/dashboard/admin/layers/mapping", params)
    .then((r) => r);
}

export function getAttributeValues(alias) {
  return axiosDashboard
    .get(`api/v1/dashboard/admin/layers/mapping/${alias}/attributeValues`)
    .then((r) => r.data);
}

export function getLegendData(params) {
  return axiosDashboard
    .get("/api/v1/dashboard/legends", {
      params,
    })
    .then((r) => r.data);
}

export function getLevels() {
  return axiosDashboard.get("/api/v1/dashboard/legends/level_type").then((r) => r.data);
}

export function editLegend(params) {
  return axiosDashboard.post("/api/v1/dashboard/legends/save", params).then((r) => r);
}

export function deleteLegend(id) {
  return axiosDashboard.delete(`/api/v1/dashboard/legends/${id}`).then((r) => r);
}

export function getSeasons() {
  return axiosDashboard.get("/api/v1/dashboard/legends/season_type").then((r) => r.data);
}

export function getProperties(types) {
  return axiosDashboard.get(`api/v1/dashboard/property/?types=${types.toString()}`);
}

export function getCameraSettings() {
  return getProperties([PropertyType.CAMERA]);
}

export function getDashboardSettings() {
  return getProperties([PropertyType.DASHBOARD]);
}

export function updateProperty(property) {
  return axiosDashboard.patch(`/api/v1/dashboard/property/`, {...property});
}

export const adminGetActiveFilter = () => {
  return axiosDashboard.get(URL_ADMIN_FILTER).then(({data}) => data);
};

export const adminPostActiveFilter = (activeFilterDTO) => {
  return axiosDashboard.post(URL_ADMIN_FILTER, activeFilterDTO).then(({data}) => data);
};
