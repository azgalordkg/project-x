import {axiosDashboard} from "./axios";

import {URL_SAVE_REPORT, URL_INSTRUCTION} from "./constants";

export const addReport = (params) => axiosDashboard.post(URL_SAVE_REPORT, params);

export const uploadFileReport = (formData) =>
  axiosDashboard.post("/egip/files", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });

// не нашел в сваггере
export const getInstructionData = () => axiosDashboard.get(URL_INSTRUCTION);
