import * as React from "react";

import AbsoluteBottomRight from "./AbsoluteBottomRight";

const VersionLabel = React.memo(() => {
  return <AbsoluteBottomRight>{process.env._VERSION_}</AbsoluteBottomRight>;
});

VersionLabel.displayName = "VersionLabel";

export default VersionLabel;
