import React from "react";

const AbsoluteBottomRight = ({children}) => (
  <div className="absolute-bottom-right">{children}</div>
);

export default AbsoluteBottomRight;
