import * as React from "react";

const LoadingSpinSvg = React.memo(({loadingWidth, overall, backgroundColor}) => {
  return (
    <div
      style={{zIndex: overall && 100000000, background: backgroundColor ?? "inherit"}}
      className="spin-container-wrap"
    >
      <div
        className="spin-container"
        style={{width: loadingWidth ?? "100px", height: loadingWidth ?? "100px"}}
      >
        <svg className="spin" viewBox="0 0 50 50">
          <circle className="spin-circle-background" cx="25" cy="25" r="20" fill="none" />
          <circle className="spin-circle" cx="25" cy="25" r="20" fill="none" />
        </svg>
      </div>
    </div>
  );
});

LoadingSpinSvg.displayName = "LoadingSpinSvg";

export default LoadingSpinSvg;
