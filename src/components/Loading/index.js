import * as React from "react";
import {LoadingOutlined} from "@ant-design/icons";

const LoadingSpinSvg = React.lazy(() =>
  import(/* webpackChunkName: "LoadingSpinSvg" */ "./components/SpinSvg")
);

const Loading = React.memo(({type, loadingWidth, overall, backgroundColor}) => {
  if (type === "new_spinner") {
    return (
      <React.Suspense fallback={<div />}>
        <LoadingSpinSvg
          loadingWidth={loadingWidth}
          overall={overall}
          backgroundColor={backgroundColor}
        />
      </React.Suspense>
    );
  }

  return <LoadingOutlined className="icon-wrap" spin />;
});

Loading.displayName = "Loading";

export default Loading;
