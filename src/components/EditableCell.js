import * as React from "react";
import {Form, Input, InputNumber, Checkbox, DatePicker as DatePickerAntd} from "antd";
import {getCurrentMomentDate} from "../utils/dates";
import {DataType} from "../consts/types";

const DatePicker = DatePickerAntd;
const FormItem = Form.Item;
const EditableContext = React.createContext(undefined);

// eslint-disable-next-line no-unused-vars
const EditableRow = ({form, index, ...props}) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

export const EditableFormRow = Form.create()(EditableRow);

export class EditableCell extends React.Component {
  state = {
    editing: false,
    calendarOpen: false,
  };

  formItemElement;
  form;
  cell;

  componentDidMount() {
    if (this.props.editable) {
      document.addEventListener("click", this.handleClickOutside);
    }
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleClickOutside);
  }

  toggleEdit = (val) => {
    const editing = val;
    this.setState({editing}, () => {
      if (editing && this.formItemElement && "focus" in this.formItemElement) {
        this.formItemElement.focus();
        if (this.props.onEditing) {
          this.props.onEditing();
        }
      }
    });
  };

  handleClickOutside = (e) => {
    const {editing, calendarOpen} = this.state;

    if (
      editing &&
      !this.cell.contains(e.target) &&
      !calendarOpen &&
      !e.target.className.includes("dropdown") &&
      !e.target.className.includes("checkbox")
    ) {
      this.onClickConfirm();
    }
  };

  save = () => {
    const {record, handleSave, inputType} = this.props;
    this.form.validateFields((error, values) => {
      const vals = Object.keys(values).reduce((agg, k) => {
        const v = values[k];
        if (inputType === DataType.BOOLEAN) {
          agg[k] = String(v);
          return agg;
        }

        agg[k] = v;
        return agg;
      }, {});
      handleSave({...record, ...vals}, vals);
    });
  };

  onClickTextCell = () => {
    setTimeout(() => {
      this.toggleEdit(true);
    });
  };

  onClickConfirm = () => {
    setTimeout(() => {
      this.save();
      this.toggleEdit(false);
    }, 0);
  };

  onOpenChangeCalendar = (status) => {
    setTimeout(() => {
      this.setState({
        calendarOpen: status,
      });
    }, 0);
  };

  onClickConfirmInput = (e) => {
    if (e.key === "Enter") {
      this.onClickConfirm();
    }
  };

  getInput = (type) => {
    if (type === DataType.INTEGER || type === DataType.DECIMAL) {
      return (
        <InputNumber
          ref={(node) => (this.formItemElement = node)}
          min={-Infinity}
          max={Infinity}
          size="small"
          style={{width: "100%"}}
          formatter={(value) => `${value}`.replace(/\D/g, "")}
          onKeyUp={this.onClickConfirmInput}
          {...(type === DataType.INTEGER
            ? {
                precision: 0,
              }
            : {})}
        />
      );
    }

    if (type === DataType.BOOLEAN) {
      return <Checkbox ref={(node) => (this.formItemElement = node)} />;
    }

    if (type === DataType.DATE) {
      const format = "DD-MM-YYYY";
      return (
        <DatePicker
          ref={(node) => (this.formItemElement = node)}
          format={format}
          size="small"
          onOk={this.onClickConfirm}
          onOpenChange={this.onOpenChangeCalendar}
          showTime
        />
      );
    }

    if (type === "textarea") {
      return (
        <Input.TextArea
          autosize
          ref={(node) => (this.formItemElement = node)}
          onPressEnter={this.onClickConfirm}
        />
      );
    }

    return (
      <Input
        ref={(node) => (this.formItemElement = node)}
        size="small"
        onPressEnter={this.onClickConfirm}
      />
    );
  };

  render() {
    const {editing} = this.state;
    const {
      editable,
      dataIndex,
      title,
      record,
      handleSave,
      inputType,
      dateFormat,
      col = {},
      formItemProps,
      onEditing,
      ...restProps
    } = this.props;
    const isDate = col?.data && col.data.type === DataType.DATE;
    const isBool = col?.data && col.data.type === DataType.BOOLEAN;

    function initialValue(record, dataIndex) {
      if (isDate) {
        return record[dataIndex] ? getCurrentMomentDate(record[dataIndex]) : null;
      }
      if (isBool) {
        const isTrue = String(record[dataIndex]) === "true";
        return editable && isTrue;
      }
      return record[dataIndex];
    }
    return (
      <td className="cell-td" {...restProps} ref={(node) => (this.cell = node)}>
        {editable ? (
          <EditableContext.Consumer>
            {(form) => {
              this.form = form;
              return editing ? (
                <div className="cell-container">
                  <FormItem className="cell-form-item">
                    {form.getFieldDecorator(dataIndex, {
                      initialValue: initialValue(record, dataIndex),
                      valuePropName: isBool ? "checked" : "value",
                      ...formItemProps,
                    })(this.getInput(inputType, record[dataIndex]))}
                  </FormItem>
                </div>
              ) : (
                <div
                  className="editable-cell-value-wrap cell-value"
                  onClick={this.onClickTextCell}
                >
                  {restProps.children}
                </div>
              );
            }}
          </EditableContext.Consumer>
        ) : (
          restProps.children
        )}
      </td>
    );
  }
}
