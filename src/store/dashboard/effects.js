import {select, call, put, fork, takeLatest, putResolve} from "redux-saga/effects";
import {
  getDashboardData,
  getInitialFilters,
  getDashboardDashletData,
  getExecutors,
  getSubordinationTypeRepresentatives,
} from "../../api/dashboard";
import {
  selectPrimaryFilters,
  selectSecondaryFiltersWithSearch,
  selectModuleSidebarViolationTypeIsWithSystematic,
} from "../sidebar/selectors";
import {
  setDashboardData,
  setDashboardStateMachine,
  changeLastTouchFilter,
  setDashleatData,
  updateDashboardStateMachine,
  setDashboardExecutors,
  setDashboardSubordinationTypeRepresentatives,
} from "./actions";
import createEffect from "../utils/promise_middleware/create_effect";
import DASHBOARD_TYPES from "./action-types";
import {
  preparePrimaryFilters,
  prepareFilters,
  filterDashboardFiltersByActive,
  wrapArrItemInObj,
} from "../utils/dashboard.utils";
import {setInitialFilters, changeSecondaryFilters} from "../sidebar/actions";
import {States} from "../../consts/store/types";
import {
  selectDashboardData,
  selectLastTouchFilter,
  selectDashboardObjectWidget,
  selectDashboardStateMachine,
} from "./selectors";
import {sortNumber} from "../../utils/common.utils";
import isEqual from "lodash-es/isEqual";

const excludeAlias = new Set(["objects", "useObjectAsPercentDivider"]);

function* onGetDashboardDataEffect({payload}) {
  const primaryFilters = yield select(selectPrimaryFilters);
  const secondaryFilters = yield select(selectSecondaryFiltersWithSearch);
  const lastTouchedFilter = yield select(selectLastTouchFilter);
  const violationTypeIsSys = yield select(
    selectModuleSidebarViolationTypeIsWithSystematic
  );

  const getParams = preparePrimaryFilters(primaryFilters, payload.dateFormat);
  const bodyParams = prepareFilters(secondaryFilters);

  const stateMachineOne = yield select(selectDashboardStateMachine);
  const stateMachineWithFetching = {...stateMachineOne};

  Object.keys(secondaryFilters).map((key) => {
    if ((lastTouchedFilter && lastTouchedFilter === key) || violationTypeIsSys) {
      stateMachineWithFetching[key] = States.SUCCESS;
    } else {
      stateMachineWithFetching[key] = States.FETCHING;
    }
  });

  yield putResolve(setDashboardStateMachine(stateMachineWithFetching));

  const response = yield call(getDashboardData, bodyParams, getParams);
  const objectsData = yield select(selectDashboardObjectWidget);
  const data = response?.data;

  const stateMachineTwo = yield select(selectDashboardStateMachine);
  const stateMachineWithSuccess = {...stateMachineTwo};

  Object.keys(secondaryFilters).map((key) => {
    if (key !== "dashleatData") {
      stateMachineWithSuccess[key] = States.SUCCESS;
    }
  });

  const currentDashboardData = yield select(selectDashboardData);
  if (lastTouchedFilter && !excludeAlias.has(lastTouchedFilter)) {
    data[lastTouchedFilter] = currentDashboardData[lastTouchedFilter];
  }

  if (violationTypeIsSys) {
    data.sources = currentDashboardData.sources;
  }

  yield put(
    setDashboardData({
      ...data,
      objects: objectsData,
    })
  );

  yield put(setDashboardStateMachine(stateMachineWithSuccess));
  yield put(changeLastTouchFilter(null));
}

function* onGetInitialFiltersEffect() {
  const stateMachine = {initialFilters: States.FETCHING};

  yield putResolve(updateDashboardStateMachine(stateMachine));

  const response = yield call(getInitialFilters);
  const data = response?.data;

  const resultObj = Object.entries(prepareFilters(data)).reduce(
    (newObj, [key, arrValue]) => {
      if (key !== "search" && key !== "useObjectAsPercentDivider") {
        try {
          const allActive =
            arrValue.filter(({active}) => !active).length === arrValue.length ||
            arrValue.filter(({active}) => active).length === arrValue.length;

          if (allActive) {
            newObj[key] = [];
          } else {
            newObj[key] = arrValue.filter(({active}) => active);
          }
        } catch {
          newObj[key] = arrValue;
        }
      } else {
        newObj[key] = arrValue;
      }

      return newObj;
    },
    {}
  );

  yield put(setInitialFilters(resultObj));
  yield putResolve(updateDashboardStateMachine({initialFilters: States.SUCCESS}));

  const secondaryFiltersOwn = yield select(selectSecondaryFiltersWithSearch);
  const secondaryFilters = filterDashboardFiltersByActive(resultObj);

  if (!isEqual(secondaryFiltersOwn, secondaryFilters)) {
    yield put(
      changeSecondaryFilters({
        ...secondaryFilters,

        search: secondaryFiltersOwn.search,
      })
    );
  }
  // const currentDashboardData: ReturnType<typeof selectDashboardData> = yield select(selectDashboardData);

  // yield put(setDashboardData({
  //   ...currentDashboardData,
  //   sources: resultObj.sources,
  // }));
}

function* onGetDashleatDataEffect({payload}) {
  const stateMachine = {
    dashleatData: States.FETCHING,
  };

  yield putResolve(updateDashboardStateMachine(stateMachine));

  const response = yield call(getDashboardDashletData, payload);
  const data = response.data.info.sort((a, b) => sortNumber(b.value, a.value));

  yield putResolve(setDashleatData(data));
  yield putResolve(updateDashboardStateMachine({dashleatData: States.SUCCESS}));
}

function* onGetExecutorsEffect() {
  const stateMachine = {
    executors: States.FETCHING,
  };

  yield putResolve(updateDashboardStateMachine(stateMachine));
  const response = yield call(getExecutors);
  // eslint-disable-next-line
  const data = wrapArrItemInObj(response.sort());

  yield putResolve(setDashboardExecutors(data));
  yield putResolve(updateDashboardStateMachine({executors: States.SUCCESS}));
}

function* onGetSubordinationTypeRepresentatives() {
  const stateMachine = {subordinationTypeRepresentatives: States.FETCHING};

  yield putResolve(updateDashboardStateMachine(stateMachine));
  const response = yield call(getSubordinationTypeRepresentatives);
  // eslint-disable-next-line
  const data = wrapArrItemInObj(response.sort());

  yield putResolve(setDashboardSubordinationTypeRepresentatives(data));
  yield putResolve(
    updateDashboardStateMachine({subordinationTypeRepresentatives: States.SUCCESS})
  );
}

function* onGetExecutorsWatcher() {
  yield fork(
    createEffect(DASHBOARD_TYPES.getInitialFilters, takeLatest, onGetExecutorsEffect)
  );
}

function* onGetSubordinationTypeRepresentativesWatcher() {
  yield fork(
    createEffect(
      DASHBOARD_TYPES.getInitialFilters,
      takeLatest,
      onGetSubordinationTypeRepresentatives
    )
  );
}

function* onGetDashboardDataWatcher() {
  yield fork(
    createEffect(DASHBOARD_TYPES.getDashboardData, takeLatest, onGetDashboardDataEffect)
  );
}

function* onGetDashboardInitialFiltersWatcher() {
  yield fork(
    createEffect(DASHBOARD_TYPES.getInitialFilters, takeLatest, onGetInitialFiltersEffect)
  );
}

function* onGetDashleatDataWatcher() {
  yield fork(
    createEffect(DASHBOARD_TYPES.getDashleatData, takeLatest, onGetDashleatDataEffect)
  );
}

// список на прослушивание
const DashboardEffects = [
  fork(onGetDashboardDataWatcher),
  fork(onGetDashboardInitialFiltersWatcher),
  fork(onGetDashleatDataWatcher),
  fork(onGetExecutorsWatcher),
  fork(onGetSubordinationTypeRepresentativesWatcher),
];

export default DashboardEffects;
