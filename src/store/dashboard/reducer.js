import {initialStateDashboard} from "./initialState";
import createReducer from "../utils/createReducer";
import DASHBOARD_TYPES from "./action-types";

export const dashboardReducer = createReducer(initialStateDashboard, {
  [DASHBOARD_TYPES.setDashboardData](state, {payload}) {
    return {
      ...state,
      data: payload,
    };
  },
  [DASHBOARD_TYPES.updateDashboardData](state, {payload}) {
    return {
      ...state,
      data: {
        ...state.data,
        ...payload,
      },
    };
  },
  [DASHBOARD_TYPES.setDashboardStateMachine](state, {payload}) {
    return {
      ...state,
      stateMachine: payload,
    };
  },
  [DASHBOARD_TYPES.updateDashboardStateMachine](state, {payload}) {
    return {
      ...state,
      stateMachine: {
        ...state.stateMachine,
        ...payload,
      },
    };
  },
  [DASHBOARD_TYPES.setDashboardObjects](state, {payload}) {
    return {
      ...state,
      data: {
        ...state.data,
        objects: payload,
      },
    };
  },
  [DASHBOARD_TYPES.setDashboardResponsibles](state, {payload}) {
    return {
      ...state,
      data: {
        ...state.data,
        subordination: payload,
      },
    };
  },
  [DASHBOARD_TYPES.setDashboardExecutors](state, {payload}) {
    return {
      ...state,
      data: {
        ...state.data,
        executors: payload,
      },
    };
  },
  [DASHBOARD_TYPES.setDashboardSubordinationTypeRepresentatives](state, {payload}) {
    return {
      ...state,
      data: {
        ...state.data,
        subordinationTypeRepresentatives: payload,
      },
    };
  },
  [DASHBOARD_TYPES.changeLastTouchFilter](state, {payload}) {
    return {
      ...state,
      lastTouchedFilter: payload,
    };
  },
  [DASHBOARD_TYPES.setDashleatData](state, {payload}) {
    return {
      ...state,
      dashleatData: payload,
    };
  },
});
