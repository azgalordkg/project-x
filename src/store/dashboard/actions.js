import DASHBOARD_TYPES from "./action-types";

export const getDashboardInitialFilters = () => {
  return {
    type: DASHBOARD_TYPES.getInitialFilters,
    payload: null,
  };
};

export const getDashboardDataAction = (dateFormat) => {
  return {
    type: DASHBOARD_TYPES.getDashboardData,
    payload: {
      dateFormat,
    },
  };
};

export const setDashboardData = (data) => {
  return {
    type: DASHBOARD_TYPES.setDashboardData,
    payload: data,
  };
};

export const updateDashboardData = (data) => {
  return {
    type: DASHBOARD_TYPES.updateDashboardData,
    payload: data,
  };
};

export const setDashboardStateMachine = (stateMachine) => {
  return {
    type: DASHBOARD_TYPES.setDashboardStateMachine,
    payload: stateMachine,
  };
};

export const updateDashboardStateMachine = (stateMachine) => {
  return {
    type: DASHBOARD_TYPES.updateDashboardStateMachine,
    payload: stateMachine,
  };
};

export const setDashboardObjects = (data) => {
  return {
    type: DASHBOARD_TYPES.setDashboardObjects,
    payload: data,
  };
};

export const setDashboardResponsibles = (data) => {
  return {
    type: DASHBOARD_TYPES.setDashboardResponsibles,
    payload: data,
  };
};

export const setDashboardExecutors = (data) => {
  return {
    type: DASHBOARD_TYPES.setDashboardExecutors,
    payload: data,
  };
};

export const setDashboardSubordinationTypeRepresentatives = (data) => {
  return {
    type: DASHBOARD_TYPES.setDashboardSubordinationTypeRepresentatives,
    payload: data,
  };
};

export const changeLastTouchFilter = (payload) => {
  return {
    type: DASHBOARD_TYPES.changeLastTouchFilter,
    payload,
  };
};

export const getDashleatData = (params) => {
  return {
    type: DASHBOARD_TYPES.getDashleatData,
    payload: params,
  };
};

export const setDashleatData = (data) => {
  return {
    type: DASHBOARD_TYPES.setDashleatData,
    payload: data,
  };
};
