import {States} from "../../consts/store/types";

export function selectModuleDashboard(state) {
  return state.dashboard;
}

export function selectDashboardData(state) {
  return selectModuleDashboard(state).data;
}

export function selectDashboardDefectWidget(state) {
  return selectDashboardData(state)?.defects;
}

export function selectDashboardEventWidget(state) {
  return selectDashboardData(state)?.events;
}

export function selectDashboardSourceWidget(state) {
  return selectDashboardData(state)?.sources;
}

export function selectDashboardRegionWidget(state) {
  return selectDashboardData(state)?.regions;
}

export function selectDashboardSourceGroupWidget(state) {
  return selectDashboardData(state)?.sourceGroups;
}

export function selectDashboardObjectWidget(state) {
  return selectDashboardData(state)?.objects;
}

export function selectDashboardResponsibleWidget(state) {
  return selectDashboardData(state)?.subordination;
}

export function selectExecutorsWidgetState(state) {
  return selectDashboardData(state)?.executors;
}

export function selectSubordinationTypeRepresentativesWidgetState(state) {
  return selectDashboardData(state)?.subordinationTypeRepresentatives;
}

export function selectDistrictsWidget(state) {
  return selectDashboardData(state)?.districts;
}

export function selectDashboardStateMachine(state) {
  return selectModuleDashboard(state).stateMachine;
}

export function selectObjectsWidgetState(state) {
  return selectDashboardStateMachine(state)?.objects;
}

export function selectEventsWidgetState(state) {
  return selectDashboardStateMachine(state)?.events;
}

export function selectSourcesWidgetState(state) {
  return selectDashboardStateMachine(state)?.sources;
}

export function selectDashleatWidgetState(state) {
  return selectDashboardStateMachine(state)?.dashleatData;
}

export function selectInitialFiltersState(state) {
  return selectDashboardStateMachine(state)?.initialFilters;
}

export function selectSourcesWidgetStateIsLoading(state) {
  return selectSourcesWidgetState(state) === States.FETCHING;
}

export function selectEventsWidgetStateIsLoading(state) {
  return selectEventsWidgetState(state) === States.FETCHING;
}

export function selectDashleatWidgetStateIsLoading(state) {
  return selectDashleatWidgetState(state) === States.FETCHING;
}

export function selectInitialFiltersStateIsLoading(state) {
  return selectInitialFiltersState(state) === States.FETCHING;
}

export function selectDefectsWidgetState(state) {
  return selectDashboardStateMachine(state)?.defects;
}

export const selectLastTouchFilter = (state) => {
  return selectModuleDashboard(state).lastTouchedFilter;
};

export const selectDashleatData = (state) => {
  return selectModuleDashboard(state).dashleatData;
};
