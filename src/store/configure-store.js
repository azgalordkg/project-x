import {createStore, applyMiddleware} from "redux";
import {createLogger} from "redux-logger";
import createSagaMiddleware from "redux-saga";
import {composeWithDevTools} from "redux-devtools-extension";
import {persistStore} from "redux-persist";

import rootReducer from "./reducer";
import rootSaga from "./sagas";

import {onError} from "./utils/app/on-error";
import {setupAction} from "./utils/setup_action";
import promiseMiddleware from "./utils/promise_middleware/middleware";

const composeEnhancers = composeWithDevTools({trace: true, traceLimit: 50});

const loggerReduxMiddleware = createLogger({
  collapsed: true,
});

export function configureStore() {
  const sagaMiddleware = createSagaMiddleware({
    /* {sagaMonitor} */ onError,
  });

  const middleware = [promiseMiddleware, sagaMiddleware];

  if (process.env.NODE_ENV !== "production") {
    middleware.push(loggerReduxMiddleware);
  }

  const store = createStore(
    rootReducer,
    {},
    composeEnhancers(applyMiddleware(...middleware))
  );

  sagaMiddleware.run(rootSaga);

  const persistor = persistStore(store);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept("./reducer", () => {
      const nextRootReducer = require("./reducer").default;
      store.replaceReducer(nextRootReducer);
    });
  }
  store.dispatch(setupAction());

  return {
    store,
    persistor,
  };
}
