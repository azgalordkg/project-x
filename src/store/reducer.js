import {combineReducers} from "redux";

import {mapReducer} from "./map/reducer";
import {appReducer} from "./app/reducer";

export const reducerMap = {
  map: mapReducer,
  app: appReducer,
};

export default combineReducers(reducerMap);
