export const initialStateApp = {
  basemaps: {
    data: [],
  },
  loggedIn: false,
  checked: false,
  login: "",
  error: "",
  user: undefined,
  permissions: [],
  passwordSettings: undefined,
  breakpoint: "DESKTOP",
  xAuthToken: null,
  modalSessionExpiredError: undefined,

  mapData: null,

  themeName: "light",

  globalLoadingCount: 0,
  showCars: false,
};
