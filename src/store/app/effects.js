import {fork, takeLatest, call, put, putResolve, select} from "redux-saga/effects";

import APP_TYPES from "./action-types";
import {onInitEffect, loadCityDataEffect} from "../utils/app/init";
import {
  onLogoutSagaEffect,
  onLoginSagaEffect,
  onLogoutBySessionExpiredSagaEffect,
  onRefreshStateOnLogoutEffect,
  onUpdateCredentialsSagaEffect,
  onApplySettingsEffect,
} from "../utils/app/auth";
import {INIT} from "../utils/setup_action";
import createEffect from "../utils/promise_middleware/create_effect";
import {getInstructionData} from "../../api/reports";
import {updateState, setStateKey, logoutBySessionExpired} from "./actions";
import {selectLogin} from "./selectors";
import {
  loginApi,
  logout,
  getBasemaps,
  getPasswordPolicy,
  postUpdateCredentials,
} from "../../api/rest";

function* onLoginEffect({payload}) {
  const {login, password} = payload;
  try {
    const response = yield call(loginApi, {
      login,
      password,
    });
    const {data, xAuthToken} = response;

    if (data?.username) {
      /*  || !response.error || response.status != 200 */
      return yield putResolve(
        updateState({
          xAuthToken,
          loggedIn: true,
          login: data.username,
          error: "",
          user: data,
          permissions: data.permissions,
        })
      );
    }
    return yield putResolve(setStateKey("error", " "));
  } catch (err) {
    return yield putResolve(setStateKey("error", err.response?.data?.message ?? " "));
  }
}

function* onLogoutEffect() {
  logout(); // не нужно ждать завершения этого запроса
  return yield put(logoutBySessionExpired());
}

function* onCheckAuthEffect() {
  const login = yield select(selectLogin);
  let isActive = false;

  if (login) {
    yield putResolve(updateState({loggedIn: true}));

    isActive = true;
  } else {
    yield putResolve(updateState({loggedIn: false}));
  }

  yield putResolve(setStateKey("checked", true));
  return isActive;
}

function* onUpdateCredentialsEffect({payload}) {
  const {login, password, firstNewPassword, secondNewPassword} = payload;
  try {
    const response = yield call(postUpdateCredentials, {
      login,
      password,
      firstNewPassword,
      secondNewPassword,
    });
    return yield putResolve(
      updateState({
        login,
        error: response,
      })
    );
  } catch (err) {
    return yield putResolve(
      setStateKey("error", err.response?.data?.message ?? err.message)
    );
  }
}
function* onFetchPasswordSettingsEffect() {
  try {
    const response = yield call(getPasswordPolicy);
    return yield putResolve(
      updateState({
        passwordSettings: response.data,
      })
    );
  } catch (err) {
    return yield putResolve(setStateKey("error", err.message));
  }
}
function* onFetchBasemapsEffect() {
  const basemaps = yield call(getBasemaps);
  yield putResolve(setStateKey("basemaps", basemaps));
  return basemaps;
}
function* onDownloadInstructionEffect({payload}) {
  const response = yield call(getInstructionData);
  const downloadLink = document.createElement("a");
  downloadLink.href = `/egip/files/${response.data}`;

  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);
}

/* ------------------------------------------------------------- */

const onLogoutSagaWatcher = createEffect(
  APP_TYPES.logoutSaga,
  takeLatest,
  onLogoutSagaEffect
);
const onLogoutBySessionExpiredSagaWatcher = createEffect(
  APP_TYPES.logoutBySessionExpiredSaga,
  takeLatest,
  onLogoutBySessionExpiredSagaEffect
);
const onRefreshStateOnLogoutWatcher = createEffect(
  APP_TYPES.refreshStateOnLogout,
  takeLatest,
  onRefreshStateOnLogoutEffect
);
const onUpdateCredentialsSagaWatcher = createEffect(
  APP_TYPES.updateCredentialsSaga,
  takeLatest,
  onUpdateCredentialsSagaEffect
);
const onApplySettingsWatcher = createEffect(
  APP_TYPES.applySettings,
  takeLatest,
  onApplySettingsEffect
);
const onLoginSagaWatcher = createEffect(
  APP_TYPES.loginSaga,
  takeLatest,
  onLoginSagaEffect
);

const onInitWatcher = function* () {
  yield fork(createEffect(INIT, takeLatest, onInitEffect));
};

const onLoadCityDataWatcher = createEffect(
  APP_TYPES.loadCityData,
  takeLatest,
  loadCityDataEffect
);

const onLoginWatcher = createEffect(APP_TYPES.login, takeLatest, onLoginEffect);
const onLogoutWatcher = createEffect(APP_TYPES.logout, takeLatest, onLogoutEffect);
const onCheckAuthWatcher = createEffect(
  APP_TYPES.checkAuth,
  takeLatest,
  onCheckAuthEffect
);
const onUpdateCredentialsWatcher = createEffect(
  APP_TYPES.updateCredentials,
  takeLatest,
  onUpdateCredentialsEffect
);
const onFetchPasswordSettingsWatcher = createEffect(
  APP_TYPES.fetchPasswordSettings,
  takeLatest,
  onFetchPasswordSettingsEffect
);

const onFetchBasemapsWatcher = createEffect(
  APP_TYPES.fetchBasemaps,
  takeLatest,
  onFetchBasemapsEffect
);

const onDownloadInstructionWatcher = createEffect(
  APP_TYPES.downloadInstruction,
  takeLatest,
  onDownloadInstructionEffect
);

// список на прослушивание
const AppEffects = [
  fork(onLogoutSagaWatcher),
  fork(onLoginSagaWatcher),
  fork(onInitWatcher),
  fork(onLoginWatcher),
  fork(onLogoutWatcher),
  fork(onLogoutBySessionExpiredSagaWatcher),
  fork(onRefreshStateOnLogoutWatcher),
  fork(onUpdateCredentialsSagaWatcher),
  fork(onApplySettingsWatcher),
  fork(onCheckAuthWatcher),
  fork(onUpdateCredentialsWatcher),
  fork(onFetchPasswordSettingsWatcher),
  fork(onFetchBasemapsWatcher),
  fork(onDownloadInstructionWatcher),
  fork(onLoadCityDataWatcher),
];

export default AppEffects;
