import pick from "lodash-es/pick";

import CONSTANTS from "../../consts/constants";
import {checkIsMer} from "../../utils/check-on-permission.utils";

export function selectAppModule(state) {
  return state.app;
}

export const selectXAuthToken = (state) => {
  return selectAppModule(state).xAuthToken;
};
export const selectModalSessionExpired = (state) =>
  selectAppModule(state).modalSessionExpiredError;

export function selectBasemaps(state) {
  const {basemaps} = selectAppModule(state);
  return basemaps && basemaps.data ? basemaps.data : [];
}

export function selectEGKOBasemapUrl(state) {
  const xs = selectBasemaps(state);
  const default2gis = xs.find((x) => x.urlTemplate.includes("2gis") && x.defaultForSrs);
  const tgis = xs.find((x) => x.urlTemplate.includes("2gis"));
  return default2gis || tgis;
}

export function selectEGKOBasemapUrlUrlTemplate(state) {
  return selectEGKOBasemapUrl(state)?.urlTemplate;
}

/** AUTH */

export function selectedLoggedIn(state) {
  return selectAppModule(state).loggedIn;
}

export function selectLogin(state) {
  return selectAppModule(state).login;
}

export function selectAuth(state) {
  const vals = selectAppModule(state);
  const r = pick(
    vals,
    Object.keys({
      loggedIn: false,
      checked: false,
      login: "",
      error: "",
      user: undefined,
      permissions: [],
      passwordSettings: undefined,
    })
  );
  return r;
}
export function selectError(state) {
  return selectAppModule(state).error;
}

export function selectPermissions(state) {
  return selectAppModule(state).permissions;
}

export function loginLoading() {
  return false; // state.loading.effects['app/loginSaga'];
}
export function selectUser(state) {
  return selectAppModule(state).user;
}

export const selectUserIsMer = (state) => {
  return checkIsMer(selectUser(state));
};

export function selectPasswordPolicy(state) {
  return selectAppModule(state).passwordSettings
    ? {
        ...(selectAppModule(state).passwordSettings?.passwordPolicy ?? {}),
        minChangedPasswordChars: selectAppModule(state).passwordSettings
          ?.minChangedPasswordChars,
      }
    : null;
}

// /** STATE */

export const selectMapData = (state) => {
  return selectAppModule(state).mapData;
};

export const selectAppThemeName = (state) => {
  return selectAppModule(state).themeName || "light";
};

export const selectAppGlobalLoadingCount = (state) => {
  return selectAppModule(state).globalLoadingCount;
};

export const selectAppGlobalIsLoading = (state) => {
  return selectAppGlobalLoadingCount(state) > CONSTANTS.COUNT.ZERO;
};

export const selectShowCars = (state) => {
  return selectAppModule(state).showCars;
};
