import storage from "redux-persist/lib/storage";
import {persistReducer} from "redux-persist";

import {initialStateApp} from "./initialState";
import createReducer from "../utils/createReducer";
import APP_TYPES from "./action-types";
import CONSTANTS from "../../consts/constants";

// тут вроде всё понятно
const appReducerRaw = createReducer(initialStateApp, {
  [APP_TYPES.update](state, {payload}) {
    return {
      ...state,
      ...payload,
    };
  },
  [APP_TYPES.set](state, {payload, key}) {
    return {
      ...state,
      [key]: payload,
    };
  },
  [APP_TYPES.incAppGlobalLoadingCount](state, {payload}) {
    return {
      ...state,
      globalLoadingCount: Math.max(
        CONSTANTS.COUNT.ZERO,
        state.globalLoadingCount + payload.incCount
      ),
    };
  },
});

const authPersistConfig = {
  key: "partialApp",
  storage,
  whitelist: [
    "xAuthToken",
    "extents77",
    "extents3857",
    "login",
    "permissions",
    "user",
    "mapData",
    "themeName",
  ],
};

export const appReducer = persistReducer(authPersistConfig, appReducerRaw);
