export const initialMapState = {
  geometry_districts: [],
  geometry_regions: [],
  geometry_violations: null,
  pointMode: "off",
  cameraSettings: {},
  technicalSettings: {},
};
