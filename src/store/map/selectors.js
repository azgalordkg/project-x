import keyBy from "lodash-es/keyBy";
import {createSelector} from "reselect";

export const selectMapModule = (state) => state.map;

export const selectRegionsGeometry = (state) => selectMapModule(state).geometry_regions;

export const selectPointMode = (state) => selectMapModule(state).pointMode;

export const selectCameraSettings = (state) => selectMapModule(state).cameraSettings;

export const selectSelectCameraEnabled = (state) => selectCameraSettings(state)?.enabled;

export const selectCameraRadius = (state) => selectCameraSettings(state)?.value;

export const selectTechnicalEnabled = (state) => selectTechnicalSettings(state)?.enabled;

export const selectDistrictsGeometry = (state) =>
  selectMapModule(state).geometry_districts;

export const selectViolationsGeometry = (state) =>
  selectMapModule(state).geometry_violations;

export const selectRegionsGeometryIndex = createSelector(
  selectRegionsGeometry,
  (geometryRegion) => {
    return keyBy(geometryRegion, (rowData) => rowData.properties.feature_id);
  }
);

export const selectDistrictsGeometryIndex = createSelector(
  selectDistrictsGeometry,
  (geometryDistrict) => {
    return keyBy(geometryDistrict, (rowData) => rowData.properties.feature_id);
  }
);

export const selectTechnicalSettings = (state) =>
  selectMapModule(state).technicalSettings;
