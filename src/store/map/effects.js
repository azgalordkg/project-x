import {fork, select, call, put, takeLatest, takeEvery} from "redux-saga/effects";

import MAP_TYPES from "./action-types";
import createEffect from "../utils/promise_middleware/create_effect";
import {setRegionGeometry, setDistrictsGeometry, setViolationsGeometry} from "./actions";
import {
  selectDistrictsGeometry,
  selectViolationsGeometry,
  selectRegionsGeometryIndex,
  selectDistrictsGeometryIndex,
} from "./selectors";
import {genILIKEFilter, genEQUALSFilter} from "../../utils/wfs.utils";
import {getFeaturesWfs} from "../../api/wfs";

function* onGetRegionsGeometryEffect({payload}) {
  const geometryRegionIndexOld = yield select(selectRegionsGeometryIndex);

  const regionsGeometryData = yield call(
    getFeaturesWfs,
    payload.layerId,
    payload.revisionId
  );

  const geometryData = regionsGeometryData.data.features;

  const isChanged = geometryData.some((dashboardFeature) => {
    const dashboardFeatureOld =
      geometryRegionIndexOld[dashboardFeature.properties.feature_id];
    if (!dashboardFeatureOld) {
      return true;
    }

    const {id: lessId, ...dashboardFeatureOther} = dashboardFeature;

    const {id: lessOldId, ...dashboardFeatureOldOther} = dashboardFeatureOld;

    return (
      !dashboardFeatureOld ||
      JSON.stringify(dashboardFeatureOldOther) !== JSON.stringify(dashboardFeatureOther)
    );
  });

  if (isChanged) {
    yield put(setRegionGeometry(geometryData));
  }
}

function* onGetDistrictsGeometryByRegionEffect({payload}) {
  const geometryDistrictsOld = yield select(selectDistrictsGeometry);
  const geometryDistrictsIndexOld = yield select(selectDistrictsGeometryIndex);

  const districtsGeometryData = yield call(
    getFeaturesWfs,
    payload.layerId,
    payload.revisionId,
    {
      filter: `(${genILIKEFilter(
        payload.filterAttributeId,
        payload.parentRegionFilterCode
      )})`,
    }
  );

  const geometryData = districtsGeometryData.data.features;

  const isChanged = geometryData.some((dashboardFeature) => {
    const dashboardFeatureOld =
      geometryDistrictsIndexOld[dashboardFeature.properties.feature_id];
    if (!dashboardFeatureOld) {
      return true;
    }

    const {id: lessId, ...dashboardFeatureOther} = dashboardFeature;

    const {id: lessOldId, ...dashboardFeatureOldOther} = dashboardFeatureOld;

    return (
      !dashboardFeatureOld ||
      JSON.stringify(dashboardFeatureOldOther) !== JSON.stringify(dashboardFeatureOther)
    );
  });

  if (isChanged) {
    const geometryDatafeatureIdSet = new Set(
      geometryData.map((rowData) => rowData.properties.feature_id)
    );

    const filtredGeometry = geometryDistrictsOld.filter(
      (dashboardFeature) =>
        !geometryDatafeatureIdSet.has(dashboardFeature.properties.feature_id)
    );

    yield put(setDistrictsGeometry([...filtredGeometry, ...geometryData]));
  }
}

function* onGetViolationsGeometryByDistrictEffect({payload}) {
  const layer = payload.layer;

  const violationData = yield call(
    getFeaturesWfs,
    layer.layer_id,
    layer.revision.revision_id,
    {filter: `(${genEQUALSFilter("district_id", payload.selectedDistrictFeatureId)})`},
    {page: 0, size: 100000}
  );
  const currentViolationsGeometry = yield select(selectViolationsGeometry);

  yield put(
    setViolationsGeometry({
      ...currentViolationsGeometry,
      [layer.alias]: violationData.data.features,
    })
  );
}

const onGetRegionsGeometryWatcher = createEffect(
  MAP_TYPES.getRegionsGeometry,
  takeLatest,
  onGetRegionsGeometryEffect
);

const onGetDistrictsGeometryByRegionWatcher = createEffect(
  MAP_TYPES.getDistrictsGeometryByRegion,
  takeLatest,
  onGetDistrictsGeometryByRegionEffect
);

const onGetViolationsGeometryByDistrictWatcher = createEffect(
  MAP_TYPES.getViolationsGeometryByDistrict,
  takeEvery,
  onGetViolationsGeometryByDistrictEffect
);

// список на прослушивание
const MapEffects = [
  fork(onGetRegionsGeometryWatcher),
  fork(onGetDistrictsGeometryByRegionWatcher),
  fork(onGetViolationsGeometryByDistrictWatcher),
];

export default MapEffects;
