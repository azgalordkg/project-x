import storage from "redux-persist/lib/storage";
import {persistReducer} from "redux-persist";

import MAP_TYPES from "./action-types";
import {initialMapState} from "./initialState";
import createReducer from "../utils/createReducer";

// тут вроде всё понятно
const mapReducerRaw = createReducer(initialMapState, {
  [MAP_TYPES.setRegionGeometry](state, {payload}) {
    return {
      ...state,
      geometry_regions: payload,
    };
  },
  [MAP_TYPES.setDistrictsGeometry](state, {payload}) {
    return {
      ...state,
      geometry_districts: payload,
    };
  },
  [MAP_TYPES.setViolationsGeometry](state, {payload}) {
    return {
      ...state,
      geometry_violations: payload,
    };
  },
  [MAP_TYPES.setRegionGeometry](state, {payload}) {
    return {
      ...state,
      geometry_regions: payload,
    };
  },
  [MAP_TYPES.setPointMode](state, {payload}) {
    return {
      ...state,
      pointMode: payload,
    };
  },
  [MAP_TYPES.setCameraSettings](state, {payload}) {
    return {
      ...state,
      cameraSettings: payload,
    };
  },
  [MAP_TYPES.setTechnicalSettings](state, {payload}) {
    return {
      ...state,
      technicalSettings: payload,
    };
  },
});

const mapPersistConfig = {
  key: "partialMap",
  storage,
  whitelist: ["geometry_regions", "geometry_districts"],
};

export const mapReducer = persistReducer(mapPersistConfig, mapReducerRaw);
