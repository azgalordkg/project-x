import MAP_TYPES from "./action-types";
import createAction from "../utils/promise_middleware/create_action";

export const setRegionGeometry = (params) => ({
  type: MAP_TYPES.setRegionGeometry,
  payload: params,
});
export const setDistrictsGeometry = (params) => ({
  type: MAP_TYPES.setDistrictsGeometry,
  payload: params,
});
export const setViolationsGeometry = (params) => ({
  type: MAP_TYPES.setViolationsGeometry,
  payload: params,
});

export const clearViolationsGeometry = () => setViolationsGeometry(null);

export const getRegionsGeometry = (layerId, revisionId) =>
  createAction({
    type: MAP_TYPES.getRegionsGeometry,
    payload: {
      layerId,
      revisionId,
    },
  });

export const getDistrictsGeometryByRegion = (
  layerId,
  revisionId,
  filterAttributeId,
  parentRegionFilterCode
) =>
  createAction({
    type: MAP_TYPES.getDistrictsGeometryByRegion,
    payload: {
      layerId,
      revisionId,
      filterAttributeId,
      parentRegionFilterCode,
    },
  });

export const getViolationsGeometryByDistrict = (layer, selectedDistrictFeatureId) =>
  createAction({
    type: MAP_TYPES.getViolationsGeometryByDistrict,
    payload: {
      layer,
      selectedDistrictFeatureId,
    },
  });

export const setMapPointMode = (mode) => ({
  type: MAP_TYPES.setPointMode,
  payload: mode,
});

export const setCameraSettings = (settings) => ({
  type: MAP_TYPES.setCameraSettings,
  payload: settings,
});

export const setTechnicalSettings = (techSettings) => ({
  type: MAP_TYPES.setTechnicalSettings,
  payload: techSettings,
});
