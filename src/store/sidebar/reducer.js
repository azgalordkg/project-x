import {isNullOrUndefined} from "util";
import createReducer from "../utils/createReducer";
import {initialStateSidebar} from "./initialState";
import SIDEBAR_TYPES from "./action-types";
import {
  getCurrentMomentDate,
  formateDate,
  getLastCleaningDate,
  checkToday21AndSubtract,
  setDateTime210000,
} from "../../utils/date.utils";
import {ViolationTypesAnalytics, Season} from "../../consts/store/types";
import {filterDashboardFiltersByActive} from "../utils/dashboard.utils";

export const sidebarReducer = createReducer(initialStateSidebar, {
  [SIDEBAR_TYPES.changeStartDate](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        startDate: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeEndDate](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        endDate: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeSidebarDate](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        startDate: payload.startDate,
        endDate: payload.endDate,
      },
    };
  },
  [SIDEBAR_TYPES.changeSeason](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        season: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeViolationType](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        violationType: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeSelectedDefects](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        defects: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeSelectedObjects](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        objects: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeSelectedResponsibles](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        subordination: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeSelectedExecutors](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        executors: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeSelectedSubordinationTypeRepresentatives](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        subordinationTypeRepresentatives: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeSelectedEvents](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        events: payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeSelectedSources](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        sources: [...payload],
      },
    };
  },
  [SIDEBAR_TYPES.changeSelectedViolationTypes](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        violation_types: payload,
      },
    };
  },

  [SIDEBAR_TYPES.changeUsePeriod](state, {payload}) {
    const newEndDate = payload.usePeriod
      ? getLastCleaningDate(state.filters.endDate)
      : getLastCleaningDate(getCurrentMomentDate());

    const newStartDate = payload.usePeriod
      ? setDateTime210000(checkToday21AndSubtract(newEndDate))
      : newEndDate;

    return {
      ...state,
      filters: {
        ...state.filters,
        usePeriod: payload.usePeriod,
        endDate: newEndDate,
        startDate: newStartDate,
      },
    };
  },

  [SIDEBAR_TYPES.changeSelectedRegions](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        regions: payload,
      },
    };
  },

  [SIDEBAR_TYPES.changeSelectedDistricts](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        districts: payload,
      },
    };
  },

  [SIDEBAR_TYPES.changeSecondaryFilters](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        ...payload,
      },
    };
  },
  [SIDEBAR_TYPES.changeSearchFilter](state, {payload}) {
    return {
      ...state,
      filters: {
        ...state.filters,
        search: state.filters.search.map((rowData) => {
          if (rowData.field in payload.searchFilterRecord) {
            return payload.searchFilterRecord[rowData.field];
          }

          if (
            (rowData.isStringSearch && rowData.isActive) ||
            !isNullOrUndefined(rowData.value)
          ) {
            return {
              ...rowData,
              value: null,
              isActive: false,
            };
          }
          return rowData;
        }),
      },
    };
  },
  [SIDEBAR_TYPES.setInitialFilters](state, {payload}) {
    return {
      ...state,
      initialFilters: {
        ...payload,
      },
    };
  },
  [SIDEBAR_TYPES.resetToInitialFilters](state) {
    return {
      ...state,
      filters: {
        ...initialStateSidebar.filters,
        startDate: formateDate(getCurrentMomentDate()),
        endDate: formateDate(getCurrentMomentDate()),
        violationType: ViolationTypesAnalytics.ANS,
        season: Season.MIXED,
        usePeriod: false,
        ...filterDashboardFiltersByActive(state.initialFilters),
        search: initialStateSidebar.filters.search,
      },
    };
  },
  [SIDEBAR_TYPES.resetToInitialFiltersWithSaveViolationAndSeason](state, {}) {
    return {
      ...state,
      filters: {
        ...initialStateSidebar.filters,
        startDate: state.filters.startDate,
        endDate: state.filters.endDate,
        violationType: state.filters.violationType,
        season: state.filters.season,
        usePeriod: state.filters.usePeriod,
        ...filterDashboardFiltersByActive(state.initialFilters),
        search: initialStateSidebar.filters.search,
      },
    };
  },
  [SIDEBAR_TYPES.setSystemTypesBlocked](state, {payload}) {
    return {
      ...state,
      isSystemTypesBlocked: payload,
    };
  },
});
