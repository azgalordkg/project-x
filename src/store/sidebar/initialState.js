import {
  Season,
  ViolationTypesAnalytics,
  AllSearchStringField,
  AllSearchButtonField,
} from "../../consts/store/types";
import {getCurrentMomentDate, formateDate} from "../../utils/date.utils";

const stringSearchArr = Object.values(AllSearchStringField).map((keySerach) => ({
  field: keySerach,
  value: null,
  isStringSearch: true,
  isButtonSearch: false,
  isActive: keySerach === AllSearchStringField.TICKET,
}));

export const ButtonTrashContainer = {
  field: AllSearchButtonField.TRASH_CONTAINERS,
  value: null,
  isStringSearch: false,
  isButtonSearch: true,
  isActive: false,
};

export const buttonSearchArr = [ButtonTrashContainer];

export const initialStateSidebar = {
  filters: {
    startDate: formateDate(getCurrentMomentDate()),
    endDate: formateDate(getCurrentMomentDate()),
    usePeriod: false,
    violationType: ViolationTypesAnalytics.ANS,
    season: Season.MIXED,

    objects: [],
    events: [],
    sources: [],
    defects: [],
    regions: [],
    subordination: [],
    executors: [],
    subordinationTypeRepresentatives: [],
    sourceGroups: [],
    districts: [],
    search: [...stringSearchArr, ...buttonSearchArr],
  },

  initialFilters: {},
  isSystemTypesBlocked: false,
};
