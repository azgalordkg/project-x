import keyBy from "lodash-es/keyBy";
import SIDEBAR_TYPES from "./action-types";

export const changeStartDate = (startDate) => {
  return {
    type: SIDEBAR_TYPES.changeStartDate,
    payload: startDate,
  };
};

export const changeEndDate = (endDate) => {
  return {
    type: SIDEBAR_TYPES.changeEndDate,
    payload: endDate,
  };
};

export const changeSidebarDate = (startDate, endDate) => {
  return {
    type: SIDEBAR_TYPES.changeSidebarDate,
    payload: {
      startDate,
      endDate,
    },
  };
};

export const changeSeason = (season) => {
  return {
    type: SIDEBAR_TYPES.changeSeason,
    payload: season,
  };
};

export const changeViolationType = (violationType) => {
  return {
    type: SIDEBAR_TYPES.changeViolationType,
    payload: violationType,
  };
};

export const changeSelectedEvents = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedEvents,
    payload,
  };
};

export const changeSelectedDefects = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedDefects,
    payload,
  };
};

export const changeSelectedObjects = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedObjects,
    payload,
  };
};

export const changeSelectedResponsibles = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedResponsibles,
    payload,
  };
};

export const changeSelectedExecutors = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedExecutors,
    payload,
  };
};

export const changeSelectedSubordinationTypeRepresentatives = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedSubordinationTypeRepresentatives,
    payload,
  };
};

export const changeSelectedSources = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedSources,
    payload,
  };
};

export const changeSelectedViolationTypes = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedViolationTypes,
    payload,
  };
};

export const changeSelectedRegions = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedRegions,
    payload,
  };
};

export const changeSelectedDistricts = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSelectedDistricts,
    payload,
  };
};

export const changeSecondaryFilters = (payload) => {
  return {
    type: SIDEBAR_TYPES.changeSecondaryFilters,
    payload,
  };
};

export const setInitialFilters = (payload) => {
  return {
    type: SIDEBAR_TYPES.setInitialFilters,
    payload,
  };
};

export const resetToInitialFilters = () => {
  return {
    type: SIDEBAR_TYPES.resetToInitialFilters,
    payload: {},
  };
};

export const resetToInitialFiltersWithSaveViolationAndSeason = () => {
  return {
    type: SIDEBAR_TYPES.resetToInitialFiltersWithSaveViolationAndSeason,
    payload: {},
  };
};

export const changeUsePeriod = (usePeriod) => {
  return {
    type: SIDEBAR_TYPES.changeUsePeriod,
    payload: {
      usePeriod,
    },
  };
};

export const changeSearchFilter = (searchFilter) => {
  const searchFilterRecord = keyBy(searchFilter, "field");

  return {
    type: SIDEBAR_TYPES.changeSearchFilter,
    payload: {
      searchFilterRecord,
    },
  };
};

export const setSystemTypesBlocked = (payload) => {
  return {
    type: SIDEBAR_TYPES.setSystemTypesBlocked,
    payload,
  };
};
