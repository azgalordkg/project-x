import {createSelector} from "reselect";
import {
  ViolationTypesAnalytics,
  ViolationTypesAPI,
  AllSearchButtonField,
} from "../../consts/store/types";
import {selectAppThemeName} from "../app/selectors";

export const selectModuleSidebar = (state) => {
  return state.sidebar;
};

export function selectModuleSidebarFilters(state) {
  return selectModuleSidebar(state).filters;
}

export const selectModuleSidebarUseObjectAsPercentDivider = (state) => {
  return selectModuleSidebarFilters(state)?.useObjectAsPercentDivider;
};

export const selectModuleSidebarViolationType = (state) => {
  return selectModuleSidebarFilters(state)?.violationType;
};
export const selectModuleSidebarViolationTypeIsWithSystematic = (state) => {
  return (
    selectModuleSidebarViolationType(state) === ViolationTypesAnalytics.ALS ||
    selectModuleSidebarViolationType(state) === ViolationTypesAPI.SCR ||
    selectModuleSidebarViolationType(state) === ViolationTypesAPI.SYS
  );
};
export const selectModuleSidebarSeason = (state) => {
  return selectModuleSidebarFilters(state)?.season;
};

export const selectModuleSidebarObjects = (state) => {
  return selectModuleSidebarFilters(state)?.objects;
};

export const selectModuleSidebarResponsibles = (state) => {
  return selectModuleSidebarFilters(state)?.subordination;
};

export const selectModuleSidebarExecutors = (state) => {
  return selectModuleSidebarFilters(state)?.executors;
};

export const selectModuleSidebarSubordinationTypeRepresentatives = (state) => {
  return selectModuleSidebarFilters(state)?.subordinationTypeRepresentatives;
};

export const selectModuleSidebarEvents = (state) => {
  return selectModuleSidebarFilters(state)?.events;
};

export const selectModuleSidebarSources = (state) => {
  return selectModuleSidebarFilters(state)?.sources;
};

export const selectModuleSidebarDefects = (state) => {
  return selectModuleSidebarFilters(state)?.defects;
};

export const selectModuleSidebarRegions = (state) => {
  return selectModuleSidebarFilters(state)?.regions;
};

export const selectModuleSidebarDistricts = (state) => {
  return selectModuleSidebarFilters(state)?.districts;
};

export const selectModuleSidebarSourceGroups = (state) => {
  return selectModuleSidebarFilters(state)?.sourceGroups;
};

export const selectModuleSidebarUsePeriod = (state) => {
  return selectModuleSidebarFilters(state)?.usePeriod;
};

export const selectModuleSidebarStartDateRaw = (state) => {
  return selectModuleSidebarFilters(state)?.startDate;
};

export const selectModuleSidebarEndDateRaw = (state) => {
  return selectModuleSidebarFilters(state)?.endDate;
};

export const selectModuleSidebarSearchFilter = (state) => {
  return selectModuleSidebarFilters(state)?.search;
};

export const selectModuleSidebarSearchStringFilter = createSelector(
  selectModuleSidebarSearchFilter,
  (search) => {
    return search.filter((rowData) => rowData.isStringSearch);
  }
);

export const selectModuleSidebarSearchButtonFilter = createSelector(
  selectModuleSidebarSearchFilter,
  (search) => {
    return search.filter((rowData) => rowData.isButtonSearch);
  }
);

export const selectModuleSidebarSearchButtonTrashContainerFilter = createSelector(
  selectModuleSidebarSearchButtonFilter,
  (buttonString) => {
    return buttonString.find(
      (rowData) => rowData.field === AllSearchButtonField.TRASH_CONTAINERS
    );
  }
);

export const selectModuleSidebarSearchActiveStringFilter = createSelector(
  selectModuleSidebarSearchStringFilter,
  (searchString) => {
    return searchString.find((rowData) => rowData.isActive) || searchString[0];
  }
);

export const selectModuleSidebarSearchFilterField = (state) => {
  return selectModuleSidebarSearchActiveStringFilter(state)?.field;
};

export const selectModuleSidebarSearchFilterValue = (state) => {
  return selectModuleSidebarSearchActiveStringFilter(state)?.value;
};

export const selectModuleSidebarStartDate = selectModuleSidebarStartDateRaw;

export const selectModuleSidebarEndDate = selectModuleSidebarEndDateRaw;

export const selectPrimaryFiltersWithoutTheme = createSelector(
  selectModuleSidebarStartDate,
  selectModuleSidebarEndDate,
  selectModuleSidebarViolationType,
  selectModuleSidebarSeason,
  selectModuleSidebarUsePeriod,
  (startDate, endDate, violationType, season, usePeriod) => {
    return {
      startDate,
      endDate,
      violationType,
      season,
      usePeriod,
    };
  }
);

export const selectPrimaryFilters = createSelector(
  selectPrimaryFiltersWithoutTheme,
  selectAppThemeName,
  (primaryFiltersWithoutTheme, themeName) => {
    return {
      ...primaryFiltersWithoutTheme,
      theme: themeName,
    };
  }
);

//Todo убрать этот костыль, когда придут бэкенщики
const crutch = (arr) => {
  return arr.map((item) => item.type);
};

export const selectSecondaryFilters = createSelector(
  selectModuleSidebarObjects,
  selectModuleSidebarResponsibles,
  selectModuleSidebarExecutors,
  selectModuleSidebarSubordinationTypeRepresentatives,
  selectModuleSidebarEvents,
  selectModuleSidebarSources,
  selectModuleSidebarDefects,
  selectModuleSidebarRegions,
  selectModuleSidebarDistricts,
  selectModuleSidebarSourceGroups,
  (
    objects,
    subordination,
    executors,
    subordinationTypeRepresentatives,
    events,
    sources,
    defects,
    regions,
    districts,
    sourceGroups
  ) => {
    // eslint-disable-next-line no-param-reassign
    executors = crutch(executors);
    // eslint-disable-next-line no-param-reassign
    subordinationTypeRepresentatives = crutch(subordinationTypeRepresentatives);

    return {
      objects,
      subordination,
      executors,
      subordinationTypeRepresentatives,
      events,
      sources,
      defects,
      regions,
      districts,
      sourceGroups,
    };
  }
);

export const selectSecondaryFiltersWithSearch = createSelector(
  selectModuleSidebarObjects,
  selectModuleSidebarResponsibles,
  selectModuleSidebarExecutors,
  selectModuleSidebarSubordinationTypeRepresentatives,
  selectModuleSidebarEvents,
  selectModuleSidebarSources,
  selectModuleSidebarDefects,
  selectModuleSidebarRegions,
  selectModuleSidebarDistricts,
  selectModuleSidebarSourceGroups,
  selectModuleSidebarSearchFilter,
  selectModuleSidebarUseObjectAsPercentDivider,
  (
    objects,
    subordination,
    executors,
    subordinationTypeRepresentatives,
    events,
    sources,
    defects,
    regions,
    districts,
    sourceGroups,
    search,
    useObjectAsPercentDivider
  ) => {
    // eslint-disable-next-line no-param-reassign
    executors = crutch(executors);
    // eslint-disable-next-line no-param-reassign
    subordinationTypeRepresentatives = crutch(subordinationTypeRepresentatives);

    return {
      objects,
      subordination,
      executors,
      subordinationTypeRepresentatives,
      events,
      sources,
      defects,
      regions,
      districts,
      sourceGroups,
      search,
      useObjectAsPercentDivider,
    };
  }
);

export const selectSecondaryFiltersWithoutRegionAndDistricts = createSelector(
  selectModuleSidebarObjects,
  selectModuleSidebarResponsibles,
  selectModuleSidebarExecutors,
  selectModuleSidebarSubordinationTypeRepresentatives,
  selectModuleSidebarEvents,
  selectModuleSidebarSources,
  selectModuleSidebarDefects,
  selectModuleSidebarSourceGroups,
  (
    objects,
    subordination,
    executors,
    subordinationTypeRepresentatives,
    events,
    sources,
    defects,
    sourceGroups
  ) => {
    // eslint-disable-next-line no-param-reassign
    executors = crutch(executors);
    // eslint-disable-next-line no-param-reassign
    subordinationTypeRepresentatives = crutch(subordinationTypeRepresentatives);

    return {
      objects,
      subordination,
      executors,
      subordinationTypeRepresentatives,
      events,
      sources,
      defects,
      sourceGroups,
    };
  }
);

export const selectInitialFilters = (state) => {
  return selectModuleSidebar(state).initialFilters;
};

export const selectInitialSources = (state) => {
  return selectInitialFilters(state).sources;
};

export const selectSystemTypesBlocked = (state) => {
  return selectModuleSidebar(state).isSystemTypesBlocked;
};
