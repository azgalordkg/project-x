import {factoryAction} from "../utils/createReducer";

const SIDEBAR = factoryAction("SIDEBAR");

// type для экшена
const changeStartDate = SIDEBAR("changeStartDate");
const changeEndDate = SIDEBAR("changeEndDate");
const changeSidebarDate = SIDEBAR("changeSidebarDate");
const changeViolationType = SIDEBAR("changeViolationType");
const changeSeason = SIDEBAR("changeSeason");

const changeSelectedObjects = SIDEBAR("changeSelectedObjects");
const changeSelectedResponsibles = SIDEBAR("changeSelectedResponsibles");
const changeSelectedExecutors = SIDEBAR("changeSelectedExecutors");
const changeSelectedSubordinationTypeRepresentatives = SIDEBAR(
  "changeSelectedSubordinationTypeRepresentatives"
);
const changeSelectedDefects = SIDEBAR("changeSelectedDefects");
const changeSelectedEvents = SIDEBAR("changeSelectedEvents");
const changeSelectedSources = SIDEBAR("changeSelectedSources");
const changeSelectedViolationTypes = SIDEBAR("changeSelectedViolationTypes");
const changeSelectedRegions = SIDEBAR("changeSelectedRegions");
const changeSelectedDistricts = SIDEBAR("changeSelectedDistricts");
const changeSecondaryFilters = SIDEBAR("changeSecondaryFilters");

const changeLastTouchFilter = SIDEBAR("changeLastTouchFilter");

const setInitialFilters = SIDEBAR("setInitialFilters");
const resetToInitialFilters = SIDEBAR("resetToInitialFilters");
const resetToInitialFiltersWithSaveViolationAndSeason = SIDEBAR(
  "resetToInitialFiltersWithSaveViolationAndSeason"
);
const changeUsePeriod = SIDEBAR("changeUsePeriod");

const changeSearchFilter = SIDEBAR("changeSearchFilter");
const setSystemTypesBlocked = SIDEBAR("setSystemTypesBlocked");

const SIDEBAR_TYPES = {
  changeStartDate,
  changeEndDate,
  changeSidebarDate,
  changeViolationType,
  changeSeason,
  changeUsePeriod,

  changeSelectedObjects,
  changeSelectedResponsibles,
  changeSelectedExecutors,
  changeSelectedSubordinationTypeRepresentatives,
  changeSelectedDefects,
  changeSelectedEvents,
  changeSelectedSources,
  changeSelectedViolationTypes,
  changeSelectedRegions,
  changeSelectedDistricts,
  changeSecondaryFilters,
  changeSearchFilter,

  changeLastTouchFilter,
  setInitialFilters,
  resetToInitialFilters,
  resetToInitialFiltersWithSaveViolationAndSeason,
  setSystemTypesBlocked,
};

export default SIDEBAR_TYPES;
