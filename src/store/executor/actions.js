import {REQUEST} from "./consts";

export const getAll = (payload) => ({
  type: REQUEST,
  payload,
});
