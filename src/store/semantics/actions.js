import SEMANTICS_TYPES from "./action-types";
import createAction from "../utils/promise_middleware/create_action";

export const update = (data) => ({
  type: SEMANTICS_TYPES.update,
  payload: {
    data,
  },
});
export const updateKey = (key, data) => ({
  type: SEMANTICS_TYPES.updateKey,
  payload: {
    key,
    data,
  },
});

export const setKey = (key, data) => ({
  type: SEMANTICS_TYPES.setKey,
  payload: {
    key,
    data,
  },
});

export const refresh = () => ({
  type: SEMANTICS_TYPES.refresh,
  payload: {},
});

// экшн получения округов
export const getRegions = () => {
  return createAction({
    type: SEMANTICS_TYPES.getRegions,
    payload: {},
  });
};

// экшн получения районов
export const getDistricts = (selectedRegionData) => {
  return createAction({
    type: SEMANTICS_TYPES.getDistricts,
    payload: {
      selectedRegionData,
    },
  });
};

// экшн на изменение района
export const changeSelectedDistrict = (selectedDistrictData) => {
  return createAction({
    type: SEMANTICS_TYPES.changeSelectedDistrict,
    payload: {
      selectedDistrictData,
    },
  });
};

// экшн на изменение округа
export const changeSelectedRegion = (selectedRegionData, isReset) => {
  return createAction({
    type: SEMANTICS_TYPES.changeSelectedRegion,
    payload: {
      selectedRegionData,
      isReset,
    },
  });
};

// экшн на получение названий слоев
export const initialRegionsMapData = () => {
  return createAction({
    type: SEMANTICS_TYPES.initialRegionsMapData,
    payload: {},
  });
};

// экшн на получение названий слоев
export const getLayerNames = () => {
  return createAction({
    type: SEMANTICS_TYPES.getLayerNames,
    payload: {},
  });
};

export const refreshSemantics = () => {
  return createAction({
    type: SEMANTICS_TYPES.refresh,
    payload: {},
  });
};

// // экшн на очистку стейта выбранного района
export const refreshSelectedDistrict = () => {
  return createAction({
    type: SEMANTICS_TYPES.refreshSelectedDistrict,
    payload: {},
  });
};

export const changeSelectedObject = (selectedObject) => {
  return createAction({
    type: SEMANTICS_TYPES.changeSelectedObject,
    payload: {
      selectedObject,
    },
  });
};

export const getLegend = (payload) => {
  return createAction({
    type: SEMANTICS_TYPES.getLegend,
    payload,
  });
};

export const showNearestCamera = (payload) => {
  return createAction({
    type: SEMANTICS_TYPES.showNearestCamera,
    payload,
  });
};

export const hideNearestCamera = () => setKey("nearestCameras", null);

export const changeDistrictIncidents = (payload) => {
  return createAction({
    type: SEMANTICS_TYPES.changeDistrictIncidents,
    payload,
  });
};

export const getIncidentsAction = (primaryFiltersOwn, secondaryFiltersOwn) => {
  return createAction({
    type: SEMANTICS_TYPES.getIncidents,
    payload: {
      primaryFiltersOwn,
      secondaryFiltersOwn,
    },
  });
};
