import {factoryAction} from "../utils/createReducer";

const SEMANTICS = factoryAction("SEMANTICS");

// type для экшена
const update = SEMANTICS("update");
const updateKey = SEMANTICS("updateKey");
const setKey = SEMANTICS("setKey");
const refresh = SEMANTICS("refresh");

const getRegions = SEMANTICS("getRegions");
const getDistricts = SEMANTICS("getDistricts");
const changeSelectedDistrict = SEMANTICS("changeSelectedDistrict");
const changeSelectedRegion = SEMANTICS("changeSelectedRegion");
const initialRegionsMapData = SEMANTICS("initialRegionsMapData");
const getLayerNames = SEMANTICS("getLayerNames");
const refreshSemantics = SEMANTICS("refreshSemantics");
const refreshSelectedDistrict = SEMANTICS("refreshSelectedDistrict");
const changeSelectedObject = SEMANTICS("changeSelectedObject");
const getLegend = SEMANTICS("getLegend");
const showNearestCamera = SEMANTICS("showNearestCamera");

//new api
const changeDistrictIncidents = SEMANTICS("changeDistrictIncidents");

const getIncidents = SEMANTICS("getIncidents");

const SEMANTICS_TYPES = {
  update,
  updateKey,
  setKey,
  refresh,
  getRegions,
  getDistricts,
  changeSelectedDistrict,
  changeSelectedRegion,
  initialRegionsMapData,
  getLayerNames,
  refreshSelectedDistrict,
  changeSelectedObject,
  getLegend,
  showNearestCamera,
  refreshSemantics,
  changeDistrictIncidents,
  getIncidents,
};

export default SEMANTICS_TYPES;
