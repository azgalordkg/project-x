export const factoryAction = (key) => (type) => {
  return `${key}/${type}`;
};

const createReducer = (initialState, handlers) => {
  return (state = initialState, action = {}) => {
    if (handlers && handlers[action.type]) {
      return handlers[action.type](state, action);
    } else {
      return state;
    }
  };
};

export default createReducer;
