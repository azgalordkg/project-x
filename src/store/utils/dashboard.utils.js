import {
  ViolationTypesAnalytics,
  ViolationTypesAPI,
  Season,
} from "../../consts/store/types";
import {getFormattedDateTimeStandart} from "../../utils/date.utils";
import omit from "lodash-es/omit";
import {isBoolean, isNullOrUndefined} from "util";
import {AllSearchButtonField} from "../../consts/store/types";

function setViolationTypeForPrimaryFiltersForSocket(primaryFilters) {
  if (primaryFilters.violationType === ViolationTypesAnalytics.ANS) {
    return [ViolationTypesAPI.CMN, ViolationTypesAPI.CRT];
  }
  if (primaryFilters.violationType === ViolationTypesAnalytics.ALS) {
    return [ViolationTypesAPI.SYS, ViolationTypesAPI.SCR];
  }
  return [primaryFilters.violationType];
}

export const preparePrimaryFilters = (primaryFilters) => {
  return {
    ...primaryFilters,
    violationType: setViolationTypeForPrimaryFiltersForSocket(primaryFilters).join(","),
    season: primaryFilters.season === Season.MIXED ? null : primaryFilters.season,
    startDate: getFormattedDateTimeStandart(primaryFilters.startDate),
    endDate: getFormattedDateTimeStandart(primaryFilters.endDate),
  };
};

export const preparePrimaryFiltersForSocket = (primaryFilters) => {
  const primaryFiltersForSocket = {
    ...primaryFilters,
    violationTypes: setViolationTypeForPrimaryFiltersForSocket(primaryFilters),
    season:
      primaryFilters.season === Season.MIXED
        ? [Season.WINTER, Season.ALL]
        : [primaryFilters.season],
    startDate: getFormattedDateTimeStandart(primaryFilters.startDate),
    endDate: getFormattedDateTimeStandart(primaryFilters.endDate),
  };
  return omit(primaryFiltersForSocket, "violationType");
};

export const prepareFilters = (object) => {
  let result = {};

  result = filterObjectByNullAndUndefined(object);

  result.search = result.search
    .filter(({value, field}) => {
      if (field === AllSearchButtonField.TRASH_CONTAINERS) {
        return value;
      }

      return !isNullOrUndefined(value);
    })
    .map(({field, value}) => ({field, value}));

  return result;
};

export const filterObjectByNullAndUndefined = (object) => {
  let result = {};
  Object.keys(object).map((key) => {
    if (typeof object[key] !== "undefined" && object[key] !== null) {
      result[key] = object[key];
    }
  });
  return result;
};

export const filterDashboardFiltersByActive = (object) => {
  return Object.entries(object).reduce((newObj, [filterKey, filterValues]) => {
    if (isBoolean(filterValues)) {
      newObj[filterKey] = filterValues;
    } else {
      newObj[filterKey] = filterValues?.filter(({active}) => active) ?? [];
    }
    return newObj;
  }, {});
};

export const wrapArrItemInObj = (arr) => {
  return arr.map((item) => ({type: item, description: item}));
};
