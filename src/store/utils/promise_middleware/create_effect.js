import {call} from "redux-saga/effects";
import {PromiseMeta} from "./constant";

const createEffect = (actionType, takeFunc, effect) => {
  function* wrapEffect(action) {
    const ans = yield call(effect, action);
    action?.[PromiseMeta]?.resolve?.(ans);
  }

  return function* () {
    yield takeFunc(actionType, wrapEffect);
  };
};

export default createEffect;
