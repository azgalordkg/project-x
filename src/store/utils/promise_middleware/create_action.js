import {PromiseMeta} from "./constant";

const createAction = (action) => {
  let resolve = null;
  const promise = new Promise((res) => (resolve = res));

  return {...action, [PromiseMeta]: {promise, resolve}};
};

export default createAction;
