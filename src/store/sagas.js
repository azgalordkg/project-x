import {all} from "redux-saga/effects";

import MapEffects from "./map/effects";
import AppEffects from "./app/effects";

export default function* root() {
  yield all([...MapEffects, ...AppEffects]);
}
