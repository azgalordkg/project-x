import ANGLE from "./angle";
import COUNT from "./count";
import INDEX_OF_ARR from "./index_of_arr";
import PERCENT from "./percent";
import TIME from "./time";

const CONSTANTS = {
  ANGLE,
  COUNT,
  INDEX_OF_ARR,
  PERCENT,
  TIME,
};

export default CONSTANTS;
