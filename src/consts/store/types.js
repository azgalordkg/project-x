export const Season = {
  ALL: "all",
  WINTER: "winter",
  MIXED: "mixed",
};

export const PointMode = {
  OFF: "off",
  SEARCH: "search",
  SELECTED: "selected",
};

export const ViolationTypesAPI = {
  CMN: "VIOLATION_NOT_CRITICAL",
  CRT: "VIOLATION_CRITICAL",
  SYS: "SYSTEMATIC_NOT_CRITICAL",
  SCR: "SYSTEMATIC_CRITICAL",
};

export const ViolationTypesAnalytics = {
  ANS: "ALL_NOT_SYSTEMATIC",
  ALS: "ALL_SYSTEMATIC",
};

export const ViolationTypesSidebar = {
  [ViolationTypesAPI.CMN]: "Некритичные",
  [ViolationTypesAPI.CRT]: "Критичные",
  [ViolationTypesAnalytics.ALS]: "Систематические",
};

export const States = {
  INITIAL: 0,
  FETCHING: 1,
  SUCCESS: 2,
  FAIL: 3,
};

export const AllSearchButtonField = {
  TRASH_CONTAINERS: "TRASH_CONTAINERS",
};

export const AllSearchStringField = {
  DEFECT_TEXT: "DEFECT_TEXT",
  ADDRESS: "ADDRESS",
  OBJECT_ID: "OBJECT_ID",
  TICKET: "TICKET",
};
