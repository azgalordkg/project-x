import {
  Season,
  ViolationTypesAPI,
  ViolationTypesAnalytics,
  ViolationTypesSidebar,
} from "../store/types";
import {TabId} from "../types";

export const SIDEBAR_TABS_LEFT = {
  [TabId.DASHBOARD]: {
    id: TabId.DASHBOARD,
    title: "Дашборд",
    path: "/dashboard",
  },
  [TabId.TABLE]: {
    id: TabId.TABLE,
    title: "Таблица",
    path: "/table",
  },
};

export const SIDEBAR_TABS_RIGHT = {
  [TabId.MAP]: {
    id: TabId.MAP,
    title: "Карта",
    path: "/map",
    notVisible: false,
  },
};

export const SIDEBAR_TABS = {
  ...SIDEBAR_TABS_LEFT,
  ...SIDEBAR_TABS_RIGHT,
};

export const SIDEBAR_TABS_LEFT_ENTRIES = Object.entries(SIDEBAR_TABS_LEFT);
export const SIDEBAR_TABS_RIGHT_MAP_ENTRIES = Object.entries(SIDEBAR_TABS_RIGHT);
export const SIDEBAR_TABS_ENTRIES = Object.entries(SIDEBAR_TABS);

export const SIDEBAR_SESASONS = [
  {
    id: Season.WINTER,
    title: "Зима",
  },
  {
    id: Season.ALL,
    title: "Лето",
  },
];

export const SIDEBAR_VIOLATIONS = [
  {
    id: ViolationTypesAPI.CRT,
    title: ViolationTypesSidebar[ViolationTypesAPI.CRT],
  },
  {
    id: ViolationTypesAPI.CMN,
    title: ViolationTypesSidebar[ViolationTypesAPI.CMN],
  },
  {
    id: ViolationTypesAnalytics.ALS,
    title: ViolationTypesSidebar[ViolationTypesAnalytics.ALS],
  },
];
