export const DataType = {
  STRING: "STRING",
  INTEGER: "INTEGER",
  BOOLEAN: "BOOLEAN",
  DATE: "DATE",
  DECIMAL: "DECIMAL",
  PHOTO: "PHOTO",
};

export const PropertyType = {
  CAMERA: "CAMERA",
  DASHBOARD: "DASHBOARD",
};

export const TabId = {
  MAP: "MAP",
  REPORT: "REPORT",
  ANALYTICS: "ANALYTICS",
  DASHBOARD: "DASHBOARD",
  TABLE: "TABLE",
};
